import request from '@/utils/request'
import url from 'postcss-url'

export function getBlogByUid (params) {
  return request({
    url: process.env.WEB_API + '/content/getBlogByUid',
    method: 'get',
    params
  })
}

export function getSameBlogByTagUid (params) {
  return request({
    url: process.env.WEB_API + '/content/getSameBlogByTagUid',
    method: 'get',
    params
  })
}

export function getSameBlogByBlogUid (params) {
  return request({
    url: process.env.WEB_API + '/content/getSameBlogByBlogUid',
    method: 'get',
    params
  })
}

export function praiseBlogByUid (params) {
  return request({
    url: process.env.WEB_API + '/content/praiseBlogByUid',
    method: 'get',
    params
  })
}

export function getBlogPraiseCountByUid (params) {
  return request({
    url: process.env.WEB_API + '/content/getBlogPraiseCountByUid',
    method: 'get',
    params
  })
}

export function collectBlogByUid (params) {
  return request({
    url: process.env.WEB_API + '/content/collectBlogByUid',
    method: 'get',
    params
  })
}

export function isCollectAndPraise (params) {
  return request({
    url: process.env.WEB_API + '/content/isPraiseOrCollect',
    method: 'get',
    params
  })
}