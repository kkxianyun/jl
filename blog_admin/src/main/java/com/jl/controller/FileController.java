package com.jl.controller;

import com.jl.service.FileService;
import com.jl.vo.ImageVO;
import com.jl.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author YanYong
 * @Date 2022/3/9 23:47
 */
@RestController
@CrossOrigin
@RequestMapping("/file")
public class FileController {

    @Autowired
    private FileService fileService;

    /**
     * 文件上传
     *
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public SysResult upload(MultipartFile file) {
        if (file != null) {
            ImageVO imageVO = fileService.upload(file);
            if (imageVO == null) {
                return SysResult.fail();
            }
            return SysResult.success(imageVO);
        }
        return SysResult.fail();
    }

    /**
     * 文件删除
     *
     * @param virtualPath
     * @return
     */
    @DeleteMapping("/deleteFile")
    public SysResult deleteFile(String virtualPath) {
        fileService.deleteFile(virtualPath);
        return SysResult.success();
    }

}
