package com.jl.controller;

import com.jl.pojo.Blog;
import com.jl.service.BlogService;
import com.jl.vo.PageResult;
import com.jl.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/***
 *
 *   █████▒█    ██  ▄████▄   ██ ▄█▀       ██████╗ ██╗   ██╗ ██████╗
 * ▓██   ▒ ██  ▓██▒▒██▀ ▀█   ██▄█▒        ██╔══██╗██║   ██║██╔════╝
 * ▒████ ░▓██  ▒██░▒▓█    ▄ ▓███▄░        ██████╔╝██║   ██║██║  ███╗
 * ░▓█▒  ░▓▓█  ░██░▒▓▓▄ ▄██▒▓██ █▄        ██╔══██╗██║   ██║██║   ██║
 * ░▒█░   ▒▒█████▓ ▒ ▓███▀ ░▒██▒ █▄       ██████╔╝╚██████╔╝╚██████╔╝
 *  ▒ ░   ░▒▓▒ ▒ ▒ ░ ░▒ ▒  ░▒ ▒▒ ▓▒       ╚═════╝  ╚═════╝  ╚═════╝
 *  ░     ░░▒░ ░ ░   ░  ▒   ░ ░▒ ▒░
 *  ░ ░    ░░░ ░ ░ ░        ░ ░░ ░
 *           ░     ░ ░      ░  ░
 */
@RestController
@RequestMapping("/blog")
@CrossOrigin
public class BlogController {

    @Autowired
    private BlogService blogService;

    /**
     * 业务需求：博客发布
     * 请求类型：post
     * 请求路径:/blog/isPublish
     * 请求参数 uid
     */
    @PostMapping("/isPublish")
    public SysResult isPublish(@RequestBody Blog blog) {
        blogService.isPublish(blog);
        return SysResult.success();
    }

    /**
     * 业务需求：博客删除
     * 注意：数据的假删除只更改状态
     * 请求类型：post
     * 请求路径:/blog/updateStatus
     * 请求参数 uid
     */
    @PostMapping("updateStatus")
    public SysResult updateStatus(@RequestBody Blog blog) {
        //博客是未发布状态才能删除
        if (blogService.getBlog(blog.getUid()).getIsPublish().equals("0")) {
            blogService.updateStatus(blog.getUid());
            return SysResult.success();
        }

        return SysResult.fail();
    }
  /**
   * 业务需求：编辑博客内容
   * 请求类型：post
   * 请求路径：/blog/edit
   */
  @PostMapping("/edit")
  public SysResult bolgEdit(@RequestBody Blog blog){
    blogService.editBlog(blog);
    return SysResult.success();
  }

    /**
     * 查询所有博客
     *
     * @param pageResult
     * @return
     */
    @GetMapping("/getList")
    public SysResult getBlogList(PageResult pageResult) {
        return blogService.getBlogList(pageResult);
    }
    /**
     * 新增博客
     * 请求类型：post
     * 请求路径:/blog/add
     * 注意：做新增操作的时候，create_time , update_time 为系统当前时间。
     * is_publish 发布状态，请默认插入0未发布，其他字段都是数据库设有默认值，可以不关心。
     */
    @PostMapping("/add")
    public SysResult addBlog(@RequestBody Blog blog){
        blogService.addBlog(blog);
        return SysResult.success();
    }
}
