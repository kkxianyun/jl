package com.jl.controller;

import com.jl.pojo.Tag;
import com.jl.service.TagService;
import com.jl.utils.ResultUtil;
import com.jl.utils.StringUtils;
import com.jl.vo.PageResult;
import com.jl.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tag")
@CrossOrigin    //前后端进行跨域操作
public class TagController {
    @Autowired
    private TagService tagService;

    /**
     * 增加标签
     */
    @PostMapping("/add")
    public SysResult addTag(@RequestBody Tag tag){
        tagService.addTag(tag);
        return SysResult.success();
    }
    /**
     * 编辑标签
     */
    @PostMapping("/edit")
    public SysResult editTag(@RequestBody Tag tag){
        tagService.editTag(tag);
        return SysResult.success();
    }
    /**
     * 获取标签列表
     */
    @GetMapping("/getList")
    public SysResult getList(PageResult pageResult){
        return SysResult.success(tagService.getList(pageResult));
    }

    @PostMapping("/delete")
    public SysResult deleteTag(@RequestBody Tag tag){
        tagService.deleteTag(tag.getUid());
        return SysResult.success();
    }
}
