package com.jl.controller;

import com.jl.pojo.User;
import com.jl.service.UserService;
import com.jl.utils.StringUtils;
import com.jl.vo.PageResult;
import com.jl.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author YanYong
 * @Date 2022/3/8 8:41
 */
@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 重置用户密码
     *
     * @return
     */
    @PostMapping("/resetUserPassword")
    public SysResult resetUserPassword(@RequestBody User user) {
        if (StringUtils.isEmpty(user.getUid()) || StringUtils.isEmpty(user.getPassWord())) {
            return SysResult.fail();
        }
        return userService.resetUserPassword(user);
    }

    /**
     * 业务需求：获取用户列表
     */
    @GetMapping("/getList")
    public SysResult getList(PageResult pageResult) {
        return SysResult.success(userService.usergetList(pageResult));
    }

    /**
     * 启停用户
     */
    @PostMapping("/updateStatus")
    public SysResult userUpdateStatus(@RequestBody User user) {
        return userService.userUpdateStatus(user);
    }

    /**
     * 更新用户禁言状态
     *
     * @param user
     * @return
     */
    @PostMapping("/updateCommentStatus")
    public SysResult updateCommentStatus(@RequestBody User user) {
        return userService.updateCommentStatus(user);
    }
}
