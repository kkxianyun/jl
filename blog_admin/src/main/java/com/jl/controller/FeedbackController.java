package com.jl.controller;

/***
 *
 *   █████▒█    ██  ▄████▄   ██ ▄█▀       ██████╗ ██╗   ██╗ ██████╗
 * ▓██   ▒ ██  ▓██▒▒██▀ ▀█   ██▄█▒        ██╔══██╗██║   ██║██╔════╝
 * ▒████ ░▓██  ▒██░▒▓█    ▄ ▓███▄░        ██████╔╝██║   ██║██║  ███╗
 * ░▓█▒  ░▓▓█  ░██░▒▓▓▄ ▄██▒▓██ █▄        ██╔══██╗██║   ██║██║   ██║
 * ░▒█░   ▒▒█████▓ ▒ ▓███▀ ░▒██▒ █▄       ██████╔╝╚██████╔╝╚██████╔╝
 *  ▒ ░   ░▒▓▒ ▒ ▒ ░ ░▒ ▒  ░▒ ▒▒ ▓▒       ╚═════╝  ╚═════╝  ╚═════╝
 *  ░     ░░▒░ ░ ░   ░  ▒   ░ ░▒ ▒░
 *  ░ ░    ░░░ ░ ░ ░        ░ ░░ ░
 *           ░     ░ ░      ░  ░
 */

import com.jl.pojo.Feedback;
import com.jl.service.FeedbackService;
import com.jl.vo.PageResult;
import com.jl.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 获取反馈列表
 */
@RestController
@RequestMapping("/feedback")
@CrossOrigin//实现前后端跨域
public class FeedbackController {

    @Autowired
    private FeedbackService feedbackService;

    /**
     * 获取反馈列表
     * 请求类型 GET
     * 请求路径/feedback/getList
     * 请求参数 query pageNum pageSize
     */

    @GetMapping("/getList")
    public SysResult getList(PageResult pageResult){
        return SysResult.success(feedbackService.getList(pageResult));
    }

    /**
     * 删除反馈
     * 注意：这里的删除是只把status字段的状态修改为0
     * @return
     */
    @PostMapping("/delete")
    public SysResult delete(@RequestBody Feedback feedback){
        feedbackService.delete(feedback.getUid());
        return SysResult.success();
    }
}
