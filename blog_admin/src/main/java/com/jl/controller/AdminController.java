package com.jl.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jl.pojo.Admin;
import com.jl.service.AdminService;
import com.jl.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/admin")
@CrossOrigin    //前后端进行跨域操作
public class AdminController {

    @Autowired
    private AdminService loginService;

    /**
     * 业务需求: 实现用户登录操作
     * URL地址: /user/login
     * 请求类型: post
     * 参数:    username/password  json串
     * 返回值:  SysResult对象(token密钥)
     */
    @PostMapping("/login")
    public SysResult login(@RequestBody Admin admin){
        Admin userDb = loginService.login(admin);
        //判断token是否为null
        if(userDb == null){
            return SysResult.fail(); //表示用户登录失败!!
        }
        return SysResult.success(userDb);//表示用户登录成功!!
    }
}
