package com.jl.controller;

import com.jl.pojo.Comment;
import com.jl.service.CommentService;
import com.jl.vo.PageResult;
import com.jl.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/comment")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @PostMapping("/delete")
    public SysResult delete(@RequestBody Comment comment) {
        commentService.delete(comment.getUid());
        return SysResult.success();
    }

    /**
     * 获取评论列表
     *
     * @param pageResult
     * @return
     */
    @GetMapping("/getList")
    public SysResult getList(PageResult pageResult) {
        return SysResult.success(commentService.getCommentList(pageResult));
    }

}
