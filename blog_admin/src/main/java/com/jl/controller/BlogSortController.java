package com.jl.controller;

import com.jl.pojo.Blog;
import com.jl.pojo.BlogSort;
import com.jl.service.BlogSortService;
import com.jl.vo.BlogSortVO;
import com.jl.vo.PageResult;
import com.jl.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author YanYong
 * @Date 2022/3/7 10:04
 */
@RestController
@RequestMapping("/blogsort")
@CrossOrigin
public class BlogSortController {

    @Autowired
    private BlogSortService blogSortService;


    /**
     * 业务需求: 增加博客分类
     * URL地址: /blogsort/add
     * 请求类型: post
     * 参数:    sortName/content/sort  json串
     * 返回值:  SysResult对象
     */
    @PostMapping("/add")
    public SysResult addBlogSort(@RequestBody BlogSortVO blogSortVO) {
        return blogSortService.addBlogSort(blogSortVO);
    }


    /**
     * 业务需求：编辑博客分类
     * 请求类型：post
     * 请求路径：/blogsort/edit
     * 请求参数：uid .sortName,content ,sort
     */
    @PostMapping("/edit")
    public SysResult editBlogsort(@RequestBody BlogSort blogSort) {
        blogSortService.ediBlogsort(blogSort);
        return SysResult.success();
    }

    /**
     * 业务需求：删除博客分类
     * 请求类型：post
     * 请求路径:/blogsort/delete
     * 请求参数 uid
     */
    @PostMapping("/delete")
    public SysResult deleteBlogsoet(@RequestBody BlogSort blogSort) {
        blogSortService.deleteBlogsort(blogSort.getUid());
        return SysResult.success();
    }

    /**
     * 获取博客分类列表
     * @param pageResult
     * @return
     */
    @GetMapping("/getList")
    public SysResult getList(PageResult pageResult) {
        return SysResult.success(blogSortService.getBlogSortList(pageResult));
    }




}
