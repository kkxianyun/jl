package com.jl.service;

import com.jl.pojo.Tag;
import com.jl.vo.PageResult;

import java.util.List;

public interface TagService {
    void addTag(Tag tag);

    void editTag(Tag tag);

    /**
     * 获取标签列表
     *
     * @param pageResult
     * @return
     */
    PageResult getList(PageResult pageResult);

    void deleteTag(String uid);
}
