package com.jl.service;

import com.jl.pojo.Blog;

import com.jl.pojo.Blog;
import com.jl.vo.PageResult;
import com.jl.vo.SysResult;

public interface BlogService {

    /**
     * 博客发布
     * @param blog
     */
    void isPublish(Blog blog);

    /**
     * 博客删除
     * @param uid
     */
    void updateStatus(String uid);

    /**
     * 博客新增
     * @param blog
     */
    void addBlog(Blog blog);
    /**
     * 根据uid获取博客
     * @param uid
     * @return
     */
    Blog getBlog(String uid);


  /**
   * 编辑博客
   * @param blog
   */

  void editBlog(Blog blog);
    /**
     * 获取博客列表
     *
     * @param pageResult
     * @return
     */
    SysResult getBlogList(PageResult pageResult);
}
