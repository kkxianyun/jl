package com.jl.service;

import com.jl.vo.PageResult;

public interface FeedbackService {

    /**
     * 获取反馈列表
     * @param pageResult
     * @return
     */
    PageResult getList(PageResult pageResult);

    /**
     * 删除反馈
     * @param uid
     */
    void delete(String uid);
}
