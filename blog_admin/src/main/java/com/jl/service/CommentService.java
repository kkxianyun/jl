package com.jl.service;

import com.jl.vo.PageResult;

public interface CommentService {
    void delete(String uid);

    /**
     * 获取评论列表
     *
     * @param pageResult
     * @return
     */
    PageResult getCommentList(PageResult pageResult);

}
