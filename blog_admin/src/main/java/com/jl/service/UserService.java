package com.jl.service;

import com.jl.pojo.User;
import com.jl.vo.PageResult;
import com.jl.vo.SysResult;

/**
 * @Author YanYong
 * @Date 2022/3/8 8:41
 */
public interface UserService {

    /**
     * 重置用户密码
     *
     * @param user
     * @return
     */
    SysResult resetUserPassword(User user);

    Object usergetList(PageResult pageResult);

    SysResult userUpdateStatus(User user);

    /**
     * 更新用户禁言状态
     *
     * @param user
     * @return
     */
    SysResult updateCommentStatus(User user);
}
