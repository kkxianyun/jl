package com.jl.service.impl;

import com.jl.mapper.BlogMapper;
import com.jl.pojo.Blog;
import com.jl.pojo.Blog;
import com.jl.pojo.BlogSort;
import com.jl.pojo.Tag;
import com.jl.pojo.Tag;
import com.jl.service.BlogService;
import com.jl.vo.PageResult;
import com.jl.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.List;

/***
 *
 *   █████▒█    ██  ▄████▄   ██ ▄█▀       ██████╗ ██╗   ██╗ ██████╗
 * ▓██   ▒ ██  ▓██▒▒██▀ ▀█   ██▄█▒        ██╔══██╗██║   ██║██╔════╝
 * ▒████ ░▓██  ▒██░▒▓█    ▄ ▓███▄░        ██████╔╝██║   ██║██║  ███╗
 * ░▓█▒  ░▓▓█  ░██░▒▓▓▄ ▄██▒▓██ █▄        ██╔══██╗██║   ██║██║   ██║
 * ░▒█░   ▒▒█████▓ ▒ ▓███▀ ░▒██▒ █▄       ██████╔╝╚██████╔╝╚██████╔╝
 *  ▒ ░   ░▒▓▒ ▒ ▒ ░ ░▒ ▒  ░▒ ▒▒ ▓▒       ╚═════╝  ╚═════╝  ╚═════╝
 *  ░     ░░▒░ ░ ░   ░  ▒   ░ ░▒ ▒░
 *  ░ ░    ░░░ ░ ░ ░        ░ ░░ ░
 *           ░     ░ ░      ░  ░
 */
@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogMapper blogMapper;

    /**
     * 博客发布
     */
    @Override
    public void isPublish(Blog blog) {
        Date updateTime = new Date();
        blogMapper.isPublish(blog.getUid(), updateTime, blog.getIsPublish());
    }

    /**
     * 博客删除
     * @param uid
     */
    @Override
    public void updateStatus(String uid) {
        Date updateTime = new Date();
        blogMapper.updateStatus(uid, updateTime);
    }

    /**
     * 博客新增
     * @param blog
     */
    @Override
    public void addBlog(Blog blog) {
        String uid = UUID.randomUUID().toString().replace("-", "");
        blog.setUid(uid);
        blog.setCreateTime(new Date());
        blog.setUpdateTime(new Date());
        blog.setIsPublish("0");
        List<Tag> tagList = blog.getTagList();
        StringBuilder sb = new StringBuilder();
        for (Tag tag:tagList){
            String uid1 = tag.getUid();
            sb.append(uid1);
            sb.append(",");
        }
        String taguid = sb.toString();
        taguid=taguid.substring(0,taguid.length()-1);
        blog.setTagUid(taguid);
        blogMapper.addBlog(blog);
    }

    /**
     * 根据uid获取博客
     * @param uid
     * @return
     */
    @Override
    public Blog getBlog(String uid) {
        return blogMapper.getBlog(uid);
    }

  /**
   * 编辑博客
   * @param blog
   */

  @Override
  public void editBlog(Blog blog) {
    List<Tag> tagList = blog.getTagList();
   if (tagList != null){
     String tagUid = "";
     for (Tag tag : tagList) {
       String uid = tag.getUid();
         tagUid = tagUid + uid+",";
     }
     tagUid = tagUid.substring(0, tagUid.length()-1);
      blog.setTagUid(tagUid);
   }
   blog.setUpdateTime(new Date());
    blogMapper.editBlog(blog);
  }


    /**
     * 获取博客列表
     *
     * @param pageResult
     * @return
     */
    @Override
    public SysResult getBlogList(PageResult pageResult) {
        String query = pageResult.getQuery().trim();
        // 查询标题符合query的博客总条数
        Long total = blogMapper.findBlogCount(query);

        Integer size = pageResult.getPageSize();
        Integer start = (pageResult.getPageNum() - 1) * size;

        // 根据条件分页查询博客
        List<Blog> list = blogMapper.getBlogList(query, start, size);

        if (list != null) {
            for (Blog blog : list) {
                //设置标签列表
                if (blog.getTagUid() != null) {
                    String[] tagUids = blog.getTagUid().split(",");
                    List<Tag> tagList = new ArrayList<>();
                    for (String tagUid : tagUids) {
                        Tag tag = blogMapper.getTagByUid(tagUid);
                        tagList.add(tag);
                    }
                    blog.setTagList(tagList);
                }

                // 设置标题图片
                if (blog.getFileUid() != null) {
                    String[] fileUids = blog.getFileUid().split(",");
                    List<String> photoList = new ArrayList<>();
                    String baseUrl = "";
                    for (String fileUid : fileUids) {
                        String photoUrl = baseUrl + blogMapper.getPicName(fileUid);
                        photoList.add(photoUrl);
                    }
                    blog.setPhotoList(photoList);
                }

                // 设置博客分类
                if (blog.getBlogSortUid() != null) {
                    String blogSortUid = blog.getBlogSortUid();
                    BlogSort blogSort = blogMapper.getBlogSortByUid(blogSortUid);
                    blog.setBlogSort(blogSort);
                }
            }

            pageResult.setTotal(total).setRows(list);
            return SysResult.success(pageResult);
        }
        return SysResult.fail();
    }
}
