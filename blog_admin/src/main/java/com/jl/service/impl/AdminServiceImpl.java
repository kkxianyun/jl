package com.jl.service.impl;

import com.jl.mapper.AdminMapper;
import com.jl.pojo.Admin;
import com.jl.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.UUID;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminMapper adminMapper;

    /**
     * 业务思维能力: 用户登录操作
     * 登录业务逻辑:
     *      1.将用户的密码进行加密处理
     *      2.根据用户名和密码进行数据库查询
     *        查到了: 用户名和密码正确  返回token密钥
     *        没查到: 用户名和密码错误  返回null
     *
     * @return
     */
    @Override
    public Admin login(Admin admin) {
        //1.将密码加密处理
        String password = admin.getPassword();
        String md5Pass = DigestUtils.md5DigestAsHex(password.getBytes());
        admin.setPassword(md5Pass);
        //2.根据用户名和密码查询数据库
        Admin userDB = adminMapper.findUserByUP(admin);
        if(userDB == null){
            //用户名或者密码错误
            return null;
        }
        return userDB;
    }
}
