package com.jl.service.impl;

import com.jl.mapper.RightsMapper;
import com.jl.pojo.Rights;
import com.jl.service.RightsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RightsServiceImpl implements RightsService {

    @Autowired
    private RightsMapper rightsMapper;

    /**
     * 难点: 查询1-2级的菜单,一级对象中包含二级对象
     * 操作:
     *      1.简单程度  在业务层查询多次手动封装  效率低
     *      2.正常方式  利用Sql进行关联查询      效率高
     * @return
     */
    @Override
    public List<Rights> getRightsList() {

        return rightsMapper.getRightsList();
    }
}
