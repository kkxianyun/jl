package com.jl.service.impl;

import com.jl.mapper.BlogSortMapper;
import com.jl.pojo.BlogSort;
import com.jl.service.BlogSortService;
import com.jl.utils.ResultUtil;
import com.jl.vo.BlogSortVO;
import com.jl.vo.PageResult;
import com.jl.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class BlogSortServiceImpl implements BlogSortService {

    @Autowired
    private BlogSortMapper blogSortMapper;

    //编辑博客分类
    @Override
    public void ediBlogsort(BlogSort blogSort) {
        //获取时间记录修改 时间
        blogSort.setUpdateTime(new Date());
        blogSortMapper.updateedit(blogSort);
    }

    //删除博客分类
    @Override
    public void deleteBlogsort(String uid) {
        Date updateTime = new Date();
        blogSortMapper.updateById(uid, updateTime);
    }

    /**
     * 判断博客分类是否已存在
     * 1.查询已存在：返回201
     * 2.查询不存在：新增成功博客分类
     *
     * @param blogSortVO
     * @return
     */
    @Override
    public SysResult addBlogSort(BlogSortVO blogSortVO) {

        String blogSort1 = blogSortMapper.getBlogSort(blogSortVO.getSortName());
        if (blogSort1 != null) {
            return SysResult.fail();
        } else {
            String uid = UUID.randomUUID().toString().replace("-", "");//设置随机uid
            BlogSort blogSort = new BlogSort();
            blogSort.setSortName(blogSortVO.getSortName());
            blogSort.setSort(blogSortVO.getSort());
            blogSort.setContent(blogSortVO.getContent());
            blogSort.setUid(uid);
            blogSortMapper.addBlogSort(blogSort);
            return SysResult.success();
        }
    }

    /**
     * 查询博客分类
     *
     * @param sortName
     * @return
     */
    @Override
    public String getBlogSort(String sortName) {
        blogSortMapper.getBlogSort(sortName);
        return ResultUtil.successWithMessage("查询分类成功");
    }

    /**
     * 获取博客分类列表
     *
     * @param pageResult
     * @return
     */
    @Override
    public PageResult getBlogSortList(PageResult pageResult) {
        String query = pageResult.getQuery().trim();
        // 获取记录总数
        long total = blogSortMapper.findCount(query);
        // 获取分页结果
        Integer size = pageResult.getPageSize();
        Integer start = (pageResult.getPageNum() - 1) * size;
        List<BlogSort> list = blogSortMapper.getBlogSortList(query, start, size);
        pageResult.setTotal(total).setRows(list);
        return pageResult;
    }


}
