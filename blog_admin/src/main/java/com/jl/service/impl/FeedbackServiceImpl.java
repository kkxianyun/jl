package com.jl.service.impl;

import com.jl.mapper.FeedbackMapper;
import com.jl.pojo.Comment;
import com.jl.pojo.Feedback;
import com.jl.service.FeedbackService;
import com.jl.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/***
 *
 *   █████▒█    ██  ▄████▄   ██ ▄█▀       ██████╗ ██╗   ██╗ ██████╗
 * ▓██   ▒ ██  ▓██▒▒██▀ ▀█   ██▄█▒        ██╔══██╗██║   ██║██╔════╝
 * ▒████ ░▓██  ▒██░▒▓█    ▄ ▓███▄░        ██████╔╝██║   ██║██║  ███╗
 * ░▓█▒  ░▓▓█  ░██░▒▓▓▄ ▄██▒▓██ █▄        ██╔══██╗██║   ██║██║   ██║
 * ░▒█░   ▒▒█████▓ ▒ ▓███▀ ░▒██▒ █▄       ██████╔╝╚██████╔╝╚██████╔╝
 *  ▒ ░   ░▒▓▒ ▒ ▒ ░ ░▒ ▒  ░▒ ▒▒ ▓▒       ╚═════╝  ╚═════╝  ╚═════╝
 *  ░     ░░▒░ ░ ░   ░  ▒   ░ ░▒ ▒░
 *  ░ ░    ░░░ ░ ░ ░        ░ ░░ ░
 *           ░     ░ ░      ░  ░
 */
@Service
public class FeedbackServiceImpl implements FeedbackService {

    @Autowired
    private FeedbackMapper feedbackMapper;


    @Override
    public PageResult getList(PageResult pageResult) {
        String query = pageResult.getQuery();

        // 查反馈总数
        long total = feedbackMapper.getAll(query);

        // 获取分页结果
        Integer pageSize = pageResult.getPageSize();
        Integer pageNum = (pageResult.getPageNum() - 1) * pageSize;
        List<Feedback> list = feedbackMapper.getList(query, pageNum, pageSize);
        pageResult.setTotal(total).setRows(list);
        return pageResult;
    }

    @Override
    public void delete(String uid) {
        Date updateTime = new Date();
        feedbackMapper.update(uid,updateTime);
    }
}
