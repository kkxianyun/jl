package com.jl.service.impl;

import com.jl.mapper.UserMapper;
import com.jl.pojo.User;
import com.jl.service.UserService;
import com.jl.utils.MD5Utils;
import com.jl.vo.PageResult;
import com.jl.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Author YanYong
 * @Date 2022/3/8 8:42
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 重置用户密码
     *
     * @param user
     * @return
     */
    @Override
    public SysResult resetUserPassword(User user) {
        // 查询数据库中是否有这个用户
        User user1 = userMapper.findUserByUid(user.getUid());
        if (user1 == null) {
            return SysResult.fail();
        }

        // 取出传入的重置密码并加密
        String MD5Password = MD5Utils.string2MD5(user.getPassWord());

        // 新建一个用户并设置好属性
        user.setUpdateTime(new Date());
        user.setPassWord(MD5Password);

        // 重置密码
        Integer num = userMapper.resetUserPassword(user);
        if (num != null) {
            return SysResult.success();
        }
        return SysResult.fail();
    }

    /*
     获取用户列表
     */
    @Override
    public Object usergetList(PageResult pageResult) {
        String query = pageResult.getQuery();
        // 获取记录总数
        long total = userMapper.findCount(query);
        // 获取分页结果
        Integer size = pageResult.getPageSize();
        Integer start = (pageResult.getPageNum() - 1) * size;
        List<User> list = userMapper.usergetList(query, start, size);
        pageResult.setTotal(total).setRows(list);
        return pageResult;
    }

    /*
    启停用户
     */
    @Override
    public SysResult userUpdateStatus(User user) {

        String redisToken = "token:" + user.getUid();
        if (user.getStatus() == 0) {
            stringRedisTemplate.delete(redisToken);
        }
        userMapper.userUpdateStatus(user);
        return SysResult.success();
    }

    /**
     * 用户禁言状态
     *
     * @param user
     * @return
     */
    @Override
    public SysResult updateCommentStatus(User user) {
        userMapper.updateCommentStatus(user);
        return SysResult.success();
    }
}

