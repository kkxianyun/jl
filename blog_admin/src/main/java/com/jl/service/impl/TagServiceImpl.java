package com.jl.service.impl;

import com.jl.mapper.TagMpper;
import com.jl.pojo.Comment;
import com.jl.pojo.Tag;
import com.jl.service.TagService;
import com.jl.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class TagServiceImpl implements TagService {
    @Autowired
    private TagMpper tagMpper;

    @Override
    @Transactional
    public void addTag(Tag tag) {
        Date date = new Date();
        tag.setStatus(1);
        tag.setCreateTime(date);
        tag.setUpdateTime(date);
        tag.setClickCount(0);
        tagMpper.addTag(tag);
    }

    @Override
    @Transactional
    public void editTag(Tag tag) {
        tag.setUpdateTime(new Date());
        tagMpper.editTag(tag);
    }

    @Override
    public PageResult getList(PageResult pageResult) {
        String query = pageResult.getQuery();

        // 查标签总数
        long total = tagMpper.getAll(query);

        // 获取分页结果
        Integer pageSize = pageResult.getPageSize();
        Integer pageNum = (pageResult.getPageNum() - 1) * pageSize;
        List<Tag> list = tagMpper.getList(query, pageNum, pageSize);
        pageResult.setTotal(total).setRows(list);
        return pageResult;
    }

    @Override
    public void deleteTag(String uid) {
      Date date = new Date();
      tagMpper.update(uid,date);
    }
}
