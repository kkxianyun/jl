package com.jl.service.impl;

import com.jl.mapper.CommentMapper;
import com.jl.pojo.Comment;
import com.jl.service.CommentService;
import com.jl.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentMapper commentMapper;


    @Override
    public void delete(String uid) {
        Date updateTime = new Date();
        commentMapper.update(uid, updateTime);
    }

    @Override
    public PageResult getCommentList(PageResult pageResult) {

        String query = pageResult.getQuery().trim();

        // 查评论总数
        long total = commentMapper.findCount(query);
        // 获取分页结果
        Integer size = pageResult.getPageSize();
        Integer start = (pageResult.getPageNum() - 1) * size;
        List<Comment> list = commentMapper.getCommentList(query, start, size);
        pageResult.setTotal(total).setRows(list);
        return pageResult;
    }
}
