package com.jl.service;

import com.jl.pojo.BlogSort;
import com.jl.vo.BlogSortVO;
import com.jl.vo.SysResult;
import com.jl.vo.PageResult;

public interface BlogSortService {

    void deleteBlogsort(String uid);

    void ediBlogsort(BlogSort blogSort);

    SysResult addBlogSort (BlogSortVO blogSortVO);

    String getBlogSort (String sortName);

    /**
     * 获取博客分类列表
     *
     * @param pageResult
     * @return
     */
    PageResult getBlogSortList(PageResult pageResult);


}
