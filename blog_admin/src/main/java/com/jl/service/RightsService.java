package com.jl.service;



import com.jl.pojo.Rights;

import java.util.List;


public interface RightsService {
    List<Rights> getRightsList();
}
