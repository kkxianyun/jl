package com.jl.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 全局Web配置
 */
@Configuration
public class GlobalWebAppConfigurer implements WebMvcConfigurer {

    /**
     * 添加图片虚拟路径映射
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //虚拟映射，当请求/image/**下所有请求时，映射到本地磁盘中的goodsImages文件夹中去
        registry.addResourceHandler("/image/**")
                .addResourceLocations("file:///D:/blogImages/");
    }
}
