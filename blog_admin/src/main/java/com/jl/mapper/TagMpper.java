package com.jl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jl.pojo.Tag;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface TagMpper extends BaseMapper<Tag> {

    /**
     * 查询标签总条数
     *
     * @param query
     * @return
     */
    long getAll(String query);

    void addTag(Tag tag);

    void editTag(Tag tag);

    /**
     * 获取标签列表
     * @param query
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<Tag> getList(@Param("query") String query,
                      @Param("pageNum") int pageNum,
                      @Param("pageSize") int pageSize);

    void update(@Param("uid")String uid,
                @Param("updateTime") Date updateTime);


}
