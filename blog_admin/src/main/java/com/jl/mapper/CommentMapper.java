package com.jl.mapper;

import org.apache.ibatis.annotations.Param;
import com.jl.pojo.Comment;
import java.util.Date;
import java.util.List;

public interface CommentMapper {
    void update(@Param("uid") String uid,
                @Param("updateTime")Date date);

    /**
     * 查询评论总条数
     *
     * @param query
     * @return
     */
    long findCount(String query);

    /**
     * 获取评论列表
     *
     * @param query
     * @param start
     * @param size
     * @return
     */
    List<Comment> getCommentList(@Param("query") String query,
                                 @Param("start") Integer start,
                                 @Param("size") Integer size);

}
