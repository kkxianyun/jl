package com.jl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jl.pojo.Blog;
import com.jl.pojo.BlogSort;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

import com.jl.pojo.BlogSort;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BlogSortMapper {
    void updateedit(BlogSort blogSort);

    void updateById(@Param("uid") String uid,
                    @Param("updateTime") Date updateTime);

    void addBlogSort(BlogSort blogSort);

    String getBlogSort(String sortName);

    /**
     * 获取博客分类列表
     *
     * @param query
     * @param start
     * @param pageSize
     * @return
     */
    List<BlogSort> getBlogSortList(@Param("query") String query,
                                   @Param("start") Integer start,
                                   @Param("pageSize") Integer pageSize);

    /**
     * 查询满足查询条件的博客分类的总数
     *
     * @param query
     * @return
     */
    long findCount(String query);


}
