package com.jl.mapper;

import com.jl.pojo.Picture;

/**
 * @Author YanYong
 * @Date 2022/3/10 17:13
 */
public interface PictureMapper {

    /**
     * 保存标题图片
     *
     * @param picture
     */
    void savePicture(Picture picture);
}
