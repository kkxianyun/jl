package com.jl.mapper;

import com.jl.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author YanYong
 * @Date 2022/3/8 8:41
 */
public interface UserMapper {

    /**
     * 根据用户uid查询用户信息
     *
     * @param uid
     * @return
     */
    User findUserByUid(String uid);

    /**
     * 重置用户密码
     *
     * @param user
     * @return
     */
    Integer resetUserPassword(User user);

    /**
     * 获取用户列表
     *
     * @param query
     * @return
     */
    long findCount(String query);

    List<User> usergetList(@Param("query") String query,
                           @Param("start") Integer start,
                           @Param("size") Integer size);

    /**
     * 用户状态启停修改
     *
     * @param user
     */
    void userUpdateStatus(User user);

    /**
     * 更新用户禁言状态
     *
     * @param user
     */
    void updateCommentStatus(User user);
}
