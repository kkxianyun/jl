package com.jl.mapper;

import com.jl.pojo.Blog;
import com.jl.pojo.Blog;
import com.jl.pojo.BlogSort;
import com.jl.pojo.Tag;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface BlogMapper {
    /**
     * 博客发布
     * @param uid
     * @param updateTime
     */
    void isPublish(@Param("uid") String uid,
                   @Param("updateTime") Date updateTime,
                   @Param("isPublish") String isPublish);

    /**
     * 博客删除
     * @param uid
     * @param updateTime
     */
    void updateStatus(@Param("uid") String uid,
                      @Param("updateTime") Date updateTime);

    /**
     * 博客新增
     * @param blog
     */
    void addBlog(Blog blog);

    /**
     * 根基uid查询博客
     * @param uid
     * @return
     */
    Blog getBlog(@Param("uid") String uid);


    /**
     * 查询标题符合query的博客条数
     *
     * @param query
     * @return
     */
    Long findBlogCount(String query);

    /**
     * 获取博客列表
     *
     * @param query
     * @param start
     * @param size
     * @return
     */
    List<Blog> getBlogList(@Param("query") String query,
                           @Param("start") Integer start,
                           @Param("size") Integer size);

    /**
     * 根据标签UID查询标签
     *
     * @param tagUid
     * @return
     */
    Tag getTagByUid(String tagUid);

    /**
     * 查询标题图片的pic_name
     *
     * @param fileUid
     * @return
     */
    String getPicName(String fileUid);

    /**
     * 根据博客分类UID查询博客分类
     *
     * @param blogSortUid
     * @return
     */
    BlogSort getBlogSortByUid(String blogSortUid);

  void editBlog(Blog blog);
}
