package com.jl.mapper;


import com.jl.pojo.Rights;

import java.util.List;

public interface RightsMapper {

    List<Rights> getRightsList();
}
