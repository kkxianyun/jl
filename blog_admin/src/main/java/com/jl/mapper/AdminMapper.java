package com.jl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jl.pojo.Admin;
import org.apache.ibatis.annotations.Mapper;

public interface AdminMapper extends BaseMapper<Admin> {

    Admin findUserByUP(Admin user);
}
