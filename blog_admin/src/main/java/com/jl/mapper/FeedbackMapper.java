package com.jl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jl.pojo.Feedback;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface FeedbackMapper extends BaseMapper<Feedback> {
    /**
     * 获取反馈列表
     * @param query
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<Feedback> getList(@Param("query") String query,
                           @Param("pageNum") Integer pageNum,
                           @Param("pageSize") Integer pageSize);

    /**
     * 获取反馈总数
     * @param query
     * @return
     */
    long getAll(String query);

    /**
     * 删除反馈
     * 注意：这里的删除是只把status字段的状态修改为0
     * @param uid
     * @param updateTime
     */
    void update(@Param("uid") String uid,
                @Param("updateTime")Date updateTime);
}
