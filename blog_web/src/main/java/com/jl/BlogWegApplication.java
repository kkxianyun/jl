package com.jl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogWegApplication {
    public static void main(String[] args) {
        SpringApplication.run(BlogWegApplication.class, args);
    }
}
