package com.jl.service;

import com.jl.pojo.User;
import com.jl.vo.UserVO;

import javax.servlet.http.HttpServletRequest;

public interface UserService {
    /**
     * 更改用户密码
     * @param request
     * @param oldPwd
     * @param newPwd
     * @return
     */
    public String updateUserPwd(HttpServletRequest request, String oldPwd, String newPwd);

    /**
     * 根据uuid获取当前用户
     * @param useruid
     * @return
     */
    public User getUserByUid(String useruid);

    /**
     * 根据用户名和密码查询用户
     */
    String findUserByNameAndPwd(UserVO userVO);

    /**
     * 用户注册
     */
    String register(UserVO userVO);

    /**
     * 修改用户信息
     */
    String editUser(UserVO userVO);
}
