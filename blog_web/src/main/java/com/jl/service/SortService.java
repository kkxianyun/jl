package com.jl.service;

import java.time.LocalDate;
import java.util.Date;

public interface SortService {
    Object getArticleByMonth(String monthDate);

    Object getSortList();
}
