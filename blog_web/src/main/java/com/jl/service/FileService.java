package com.jl.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Author YanYong
 * @Date 2022/3/9 23:50
 */
public interface FileService {

    /**
     * 文件上传
     *
     * @param file
     * @return
     */
    List<String> upload(MultipartFile file);

    /**
     * 文件删除
     *
     * @param virtualPath
     */
    void deleteFile(String virtualPath);
}
