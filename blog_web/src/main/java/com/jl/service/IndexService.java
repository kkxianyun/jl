package com.jl.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jl.pojo.Blog;
import com.jl.pojo.WebNavbar;

import java.util.List;

public interface IndexService {

    /**
     * 获取所有门户导航栏
     *
     * @return
     */
    public List<WebNavbar> getAllList();

    /**
     * 获取热门博客，按点击数排序
     *
     * @return
     */
    public IPage<Blog> getHotBlog();

    /**
     * 获取首页最新的博客
     * @param currentPage
     * @param pageSize
     * @return
     */
    Object getNewBloglist(long currentPage, long pageSize);

    /**
     * 博客搜索功能
     * @param keywordslong
     * @param currentPage
     * @param pageSize
     * @return
     */
    Object getBlogBySearch(String keywordslong, long currentPage, long pageSize);

    /**
     * 热门标签展现
     * @return
     */
    Object getHotTag();
}
