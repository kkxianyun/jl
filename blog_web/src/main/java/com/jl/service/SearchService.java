package com.jl.service;

/**
 * @Author YanYong
 * @Date 2022/3/3 17:13
 */
public interface SearchService {

    /**
     * 利用sql语句查询博客内容
     *
     * @param keywords
     * @param currentPage
     * @param pageSize
     * @return
     */
    Object sqlSearchBlog(String keywords, long currentPage, long pageSize);

    /**
     * 根据标签UID获取博客
     *
     * @param tagUid
     * @param currentPage
     * @param pageSize
     * @return
     */
    Object searchBlogByTagUid(String tagUid, Integer currentPage, Integer pageSize);
}
