package com.jl.service;

import com.jl.pojo.Comment;
import com.jl.vo.CommentVO;

import javax.servlet.http.HttpServletRequest;

public interface CommentService {

    /**
     * 增加评论
     * @return
     */
    String add(CommentVO commentVO);


    /**
     *删除评论
     */
    String delete(CommentVO commentVO);

    /**
     * 获取评论列表
     * @param commentVO
     * @return
     */
    String getList(CommentVO commentVO);

    /**
     * 获取用户点赞信息
     * @param
     * @return
     */

    String getPraiseListByUser(Long currentPage, Long pageSize);

    /**
     * 获取博客收藏表信息
     * @param currentPage
     * @param pageSize
     * @return
     */
    String getCollectListByUser(Long currentPage, Long pageSize);
}
