package com.jl.service;

public interface TagService {
    Object getArticleByTagUid(String tagUid, Long currentPage, Long pageSize);

    Object getTagList();
}
