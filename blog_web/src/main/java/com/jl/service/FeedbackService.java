package com.jl.service;

import com.jl.vo.FeedbackVO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface FeedbackService {
    /**
     * 获取用户的反馈
     */
    String getFeedbackList(HttpServletRequest request);

    /**
     * 提交反馈
     */
    String addFeedback(HttpServletRequest request ,FeedbackVO feedbackVO);


}
