package com.jl.service;

import com.jl.pojo.WebConfig;

public interface WebConfigService {

    /**
     * 获取网站配置信息
     */
    WebConfig getWebConfigList();
}
