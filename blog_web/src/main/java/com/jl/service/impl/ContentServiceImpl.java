package com.jl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jl.base.EPublish;
import com.jl.mapper.*;
import com.jl.pojo.*;
import com.jl.service.ContentService;
import com.jl.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName: ContentServiceImpl
 * @Description:
 * @Date: 2022/2/22 9:38
 * @Author YanYong
 * @Since JDK 1.8
 */
@Service
public class ContentServiceImpl implements ContentService {

    @Autowired
    private BlogMapper blogMapper;

    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private TagMapper tagMapper;

    @Autowired
    private BlogSortMapper blogSortMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ContentMapper contentMapper;


    /**
     * 通过uid获取blog
     *
     * @param uid
     * @param oid
     * @return
     */
    @Override
    public String getBlogByUid(String uid, Integer oid) {
        Blog blog = null;
        if (StringUtils.isNotEmpty(uid)) {
            blog = blogMapper.selectById(uid);
        } else {
            QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("oid", oid);
            queryWrapper.last("LIMIT 1");
            blog = blogMapper.selectOne(queryWrapper);
        }

        if (blog == null || blog.getStatus() == EStatus.DISABLED || EPublish.NO_PUBLISH.equals(blog.getIsPublish())) {
            return ResultUtil.result(400, "该文章已下架或被删除！");
        }

        // 设置文章版权声明
        setBlogCopyright(blog);

        // 设置博客标签
        String tagUid = blog.getTagUid();
        if (!StringUtils.isEmpty(tagUid)) {
            String[] uids = tagUid.split(SysConf.FILE_SEGMENTATION);
            List<Tag> tagList = new ArrayList<>();
            for (String uid1 : uids) {
                Tag tag = tagMapper.selectById(uid1);
                if (tag != null && tag.getStatus() != EStatus.DISABLED) {
                    tagList.add(tag);
                }
            }
            blog.setTagList(tagList);
        }

        // 获取分类
        if (blog != null && !StringUtils.isEmpty(blog.getBlogSortUid())) {
            BlogSort blogSort = blogSortMapper.selectById(blog.getBlogSortUid());
            blog.setBlogSort(blogSort);
        }

        // 获取ip地址
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        String ip = HttpUtils.getIP(request);

        // 根据ip 从Redis取出数据，判断该用户是否点击过该文章
        String key = "click:" + ip + ":" + blog.getUid();
        String clicked = stringRedisTemplate.opsForValue().get(key);

        if (StringUtils.isEmpty(clicked)) {
            // 给博客增加点击数
            blog.setClickCount(blog.getClickCount() + 1);
            blogMapper.updateById(blog);

            // 将点击情况存入redis
            stringRedisTemplate.opsForValue().set(key, blog.getClickCount().toString(), 24, TimeUnit.HOURS);
        }

        return ResultUtil.result(SysConf.SUCCESS, blog);
    }

    /**
     * 通过BlogUid获取相关的博客
     *
     * @param blogUid
     * @return
     */
    @Override
    public Object getSameBlogByBlogUid(String blogUid) {
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        Page<Blog> page = new Page<>();
        page.setCurrent(1).setSize(6);
        Blog blog = blogMapper.selectById(blogUid);
        queryWrapper.eq("blog_sort_uid", blog.getBlogSortUid())
                .eq("is_publish", EPublish.PUBLISH)
                .orderByDesc("create_time");
        Page<Blog> pagelsit = blogMapper.selectPage(page, queryWrapper);
        List<Blog> list = pagelsit.getRecords();
        //过滤掉当前的博客
        List<Blog> list1 = new ArrayList<>();
        for (Blog item : list) {
            if (item.getUid().equals(blogUid)) {
                continue;
            }
            list1.add(item);
        }
        pagelsit.setRecords(list1);
        return pagelsit;
    }

    /**
     * 根据标签Uid获取相关的博客
     *
     * @param tagUid
     * @return
     */
    @Override
    public IPage<Blog> getSameBlogByTagUid(String tagUid) {
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        Page<Blog> page = new Page<>();
        page.setCurrent(1);
        page.setSize(10);
        queryWrapper.like("tag_uid", tagUid);
        queryWrapper.orderByDesc("create_time");
        queryWrapper.eq("status", EStatus.ENABLE);
        queryWrapper.eq("is_publish", EPublish.PUBLISH);
        IPage<Blog> pageList = blogMapper.selectPage(page, queryWrapper);
        List<Blog> list = pageList.getRecords();
        list = setTagAndSortByBlogList(list);
        pageList.setRecords(list);
        return pageList;
    }

    /**
     * 给博客列表设置分类和标签
     *
     * @param list
     * @return
     */
    @Override
    public List<Blog> setTagAndSortByBlogList(List<Blog> list) {
        List<String> sortUids = new ArrayList<>();
        List<String> tagUids = new ArrayList<>();
        list.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getBlogSortUid())) {
                sortUids.add(item.getBlogSortUid());
            }
            if (StringUtils.isNotEmpty(item.getTagUid())) {
                List<String> tagUidsTemp = StringUtils.changeStringToString(item.getTagUid(), BaseSysConf.FILE_SEGMENTATION);
                for (String itemTagUid : tagUidsTemp) {
                    tagUids.add(itemTagUid);
                }
            }
        });
        Collection<BlogSort> sortList = new ArrayList<>();
        Collection<Tag> tagList = new ArrayList<>();
        if (sortUids.size() > 0) {
            sortList = blogSortMapper.selectBatchIds(sortUids);
        }
        if (tagUids.size() > 0) {
            tagList = tagMapper.selectBatchIds(tagUids);
        }
        Map<String, BlogSort> sortMap = new HashMap<>();
        Map<String, Tag> tagMap = new HashMap<>();
        sortList.forEach(item -> {
            sortMap.put(item.getUid(), item);
        });
        tagList.forEach(item -> {
            tagMap.put(item.getUid(), item);
        });
        for (Blog item : list) {

            //设置分类
            if (StringUtils.isNotEmpty(item.getBlogSortUid())) {
                item.setBlogSort(sortMap.get(item.getBlogSortUid()));
            }
            //获取标签
            if (StringUtils.isNotEmpty(item.getTagUid())) {
                List<String> tagUidsTemp = StringUtils.changeStringToString(item.getTagUid(), BaseSysConf.FILE_SEGMENTATION);
                List<Tag> tagListTemp = new ArrayList<Tag>();
                tagUidsTemp.forEach(tag -> {
                    tagListTemp.add(tagMap.get(tag));
                });
                item.setTagList(tagListTemp);
            }
        }

        return list;
    }

    /**
     * 收藏博客
     *
     * @param uid
     * @return
     */
    @Override
    public String collectBlogByUid(String uid) {

        if (StringUtils.isEmpty(uid)) {
            return ResultUtil.errorWithMessage("传入参数有误！");
        }

        ServletRequestAttributes servletRequestAttributes =
                (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();

        //获取用户uid
        String userUid = request.getAttribute(SysConf.USER_UID).toString();

        //进行博客收藏与取消收藏
        QueryWrapper<Collect> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("blog_uid", uid);
        //获取多个用户收藏的同一个博客的集合
        List<Collect> collects = contentMapper.selectList(queryWrapper);
        for (Collect collect : collects) {
            //如果其中的一个博客的收藏用户，就是此登陆用户则取消收藏。
            if (collect.getUserUid().equals(userUid)) {
                QueryWrapper<Collect> collectQueryWrapper = new QueryWrapper<>();
//                collectQueryWrapper.eq("user_uid", userUid);
                collectQueryWrapper.eq("blog_uid", uid);
                //获取这个用户的收藏博客
                Collect collect1 = contentMapper.selectOne(collectQueryWrapper);
                //根据这个用户收藏的博客唯一uid进行删除此收藏博客
                contentMapper.deleteById(collect1.getUid());
                //博客表中的收藏数减1
                Blog blog = blogMapper.selectById(uid);
                blog.setCollectCount(blog.getCollectCount() - 1);
                blogMapper.updateById(blog);
                return ResultUtil.successWithMessage("已经取消收藏");
            }
        }

        //没有重复，收藏表添加新数据
        Collect collect1 = new Collect();
        collect1.setBlogUid(uid);
        collect1.setUserUid(userUid);
        contentMapper.insert(collect1);

        //博客表中的收藏数加1
        Blog blog = blogMapper.selectById(uid);
        blog.setCollectCount(blog.getCollectCount() + 1);
        blogMapper.updateById(blog);

        return ResultUtil.successWithMessage("收藏成功");
    }

    /**
     * 设置博客版权
     *
     * @param blog
     */
    private void setBlogCopyright(Blog blog) {

        //如果是原创的话
        if (Constants.STR_ONE.equals(blog.getIsOriginal())) {
            blog.setCopyright("原创");
        } else {
            String reprintedTemplate = "转载";
            String[] variable = {blog.getArticlesPart(), blog.getAuthor()};
            String str = String.format(reprintedTemplate, variable);
            blog.setCopyright(str);
        }
    }

    /**
     * 通过 Uid 获取博客点赞数
     *
     * @param uid
     * @return
     */
    @Override
    public Integer getBlogPraiseCountByUid(String uid) {
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("uid", uid);
        Blog blog = blogMapper.selectOne(queryWrapper);
        return blog.getPraiseCount();
    }

    /**
     * 通过Uid给博客点赞
     *
     * @param uid
     * @return
     */
    @Override
    public String praiseBlogByUid(String uid) {
        if (StringUtils.isEmpty(uid)) {
            return ResultUtil.errorWithMessage("传入参数有误！");
        }

        ServletRequestAttributes servletRequestAttributes =
                (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();

        String userUid = (String) request.getAttribute(SysConf.USER_UID);
        String redisKey = "token:" + userUid;
        String redisToken = stringRedisTemplate.opsForValue().get(redisKey);
        // 如果用户登录了
        if (StringUtils.isNotEmpty(userUid) && redisToken != null) {
            QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("user_uid", userUid);
            queryWrapper.eq("blog_uid", uid);
            // type=1表示点赞
            queryWrapper.eq("type", 1);
            Comment praise = commentMapper.selectOne(queryWrapper);

            // 查到数据说明已经点赞过
            if (praise != null) {
                return ResultUtil.errorWithMessage("你已经点赞过了！");
            }

            Comment comment = new Comment();
            comment.setBlogUid(uid);
            comment.setUserUid(userUid);
            comment.setType(1);
            comment.setSource("BLOG_INFO");
            Date date = new Date();
            comment.setCreateTime(date);
            comment.setUpdateTime(date);
            String commentUid = UUID.randomUUID().toString().replace("-", "");
            comment.setUid(commentUid);
            int insert = commentMapper.insert(comment);

            // 增加博客点赞数
            Blog blog = blogMapper.selectById(uid);
            Integer praiseCount = blog.getPraiseCount();
            blog.setPraiseCount(praiseCount + 1);
            blogMapper.updateById(blog);
            if (insert != 0) {
                return ResultUtil.result(SysConf.SUCCESS, "点赞成功！");
            }

        } else {
            return ResultUtil.errorWithMessage("请先登录才能点赞！");
        }

        return ResultUtil.errorWithMessage("抱歉，点赞未能成功！");
    }

    /**
     * 对该博客是否已点赞或收藏
     *
     * @param uid
     * @return
     */
    @Override
    public String isPraiseOrCollect(HttpServletRequest request, String uid) {

        QueryWrapper<Blog> blogQueryWrapper = new QueryWrapper<>();
        blogQueryWrapper.eq("oid", uid);
        Blog blog = blogMapper.selectOne(blogQueryWrapper);
        uid = blog.getUid();

        Map<String, Boolean> praiseAndCollectMap = new HashMap<>();

        String userUid = (String) request.getAttribute(SysConf.USER_UID);

        if (userUid == null) {
            praiseAndCollectMap.put("isPraise", false);
            praiseAndCollectMap.put("isCollect", false);
            return ResultUtil.result(SysConf.SUCCESS, praiseAndCollectMap);
        }

        QueryWrapper<Comment> commentQueryWrapper = new QueryWrapper<>();
        commentQueryWrapper.eq("user_uid", userUid);
        commentQueryWrapper.eq("blog_uid", uid);
        commentQueryWrapper.eq("type", 1);
        commentQueryWrapper.eq("status", 1);

        Comment comment = commentMapper.selectOne(commentQueryWrapper);
        if (comment != null) {
            praiseAndCollectMap.put("isPraise", true);
        } else {
            praiseAndCollectMap.put("isPraise", false);
        }

        QueryWrapper<Collect> collectQueryWrapper = new QueryWrapper<>();
        collectQueryWrapper.eq("user_uid", userUid);
        collectQueryWrapper.eq("blog_uid", uid);
        collectQueryWrapper.eq("status", 1);

        Collect collect = contentMapper.selectOne(collectQueryWrapper);
        if (collect != null) {
            praiseAndCollectMap.put("isCollect", true);
        } else {
            praiseAndCollectMap.put("isCollect", false);
        }

        return ResultUtil.result(SysConf.SUCCESS, praiseAndCollectMap);
    }
}
