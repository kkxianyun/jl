package com.jl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jl.mapper.BlogMapper;
import com.jl.mapper.CommentMapper;
import com.jl.mapper.ContentMapper;
import com.jl.mapper.UserMapper;
import com.jl.pojo.Blog;
import com.jl.pojo.Collect;
import com.jl.pojo.Comment;
import com.jl.pojo.User;
import com.jl.service.CommentService;
import com.jl.utils.*;
import com.jl.vo.CommentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private BlogMapper blogMapper;
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ContentMapper contentMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 增加评论
     *
     * @param commentVO
     * @return
     */
    @Override
    public String add(CommentVO commentVO) {
        //获取用户id，进行权限校验，是否可以添加评论
        ServletRequestAttributes servletRequestAttributes =
                (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        String userUid = (String) request.getAttribute(SysConf.USER_UID);
        String tokenKey = "token:" + userUid;
        String redisToken = stringRedisTemplate.opsForValue().get(tokenKey);

        if (StringUtils.isEmpty(userUid) || redisToken == null) {
            return ResultUtil.result(SysConf.ERROR, "请先登录再评论！");
        }

        //校验博客是否开启评论功能
        if (StringUtils.isNotEmpty(commentVO.getBlogUid())) {
            String blogUid = commentVO.getBlogUid();
            Blog blog = blogMapper.selectById(blogUid);
            if (SysConf.CAN_NOT_COMMENT.equals(blog.getOpenComment())) {
                return ResultUtil.result(SysConf.ERROR, "你尚未开启评论功能!");
            }
        }

        //创建对象进行数据转换传输
        Comment comment = new Comment();
        comment.setSource(commentVO.getSource());
        comment.setBlogUid(commentVO.getBlogUid());
        comment.setContent(commentVO.getContent());
        comment.setToUserUid(commentVO.getToUserUid());

        // 当该评论不是一级评论时，需要设置一级评论UID字段
        if (StringUtils.isNotEmpty(commentVO.getToUid())) {
            comment.setToUid(commentVO.getToUid());
            Comment toComment = commentMapper.selectById(commentVO.getToUid());
            // 表示 toComment是非一级评论
            if (toComment != null && StringUtils.isNotEmpty(toComment.getFirstCommentUid())) {
                comment.setFirstCommentUid(toComment.getFirstCommentUid());
            } else {
                // 表示父评论是一级评论，直接获取UID
                comment.setFirstCommentUid(toComment.getUid());
            }
        }
        User user = userMapper.selectById(userUid);
        //判断用户评论是否超过最大字符长度
        if (commentVO.getContent().length() > SysConf.ONE_ZERO_TWO_FOUR) {
            return ResultUtil.result(SysConf.ERROR, "您的评论字数超出限制");
        }

        // 判断该用户是否被禁言
        if (user.getCommentStatus() == SysConf.ZERO) {
            return ResultUtil.result(SysConf.ERROR, "您没有发言权限！");
        }
        //判断用户传入数据是否为空字符串
        String content = comment.getContent().trim();
        if (StringUtils.isEmpty(content)) {
            return ResultUtil.result(SysConf.ERROR, "不能传入空数据！请输入正确的评论！");
        }
        //redis缓存加入未读评论
//      String redisUid = commentVO.getToUserUid();
        if (!userUid.equals(commentVO.getToUserUid())) {
            String redisKey = "userReceiveCommentCount:" + commentVO.getToUserUid();
            stringRedisTemplate.opsForValue().increment(redisKey);
        }

        comment.setUserUid(commentVO.getUserUid());
        comment.setToUid(commentVO.getToUid());
        comment.setStatus(EStatus.ENABLE);
        commentMapper.insert(comment);
        return ResultUtil.result(SysConf.SUCCESS, comment);
    }


    /**
     * 删除评论
     */

    @Override
    public String delete(CommentVO commentVO) {
        Comment comment = commentMapper.selectById(commentVO.getUid());
        //判断评论是否可以进行删除
        if (!comment.getUserUid().equals(commentVO.getUserUid())) {
            return ResultUtil.result(SysConf.ERROR, "你没有删除该数据权限");
        }
        comment.setStatus(EStatus.DISABLED);//状态为0删除评论
        comment.updateById();

        //获取该评论下的子级评论进行删除
        List<Comment> commentList = new ArrayList<>(Constants.NUM_ONE);
        commentList.add(comment);
        //判断删除的评论是子级还是一级
        String commentUid = "";
        if (StringUtils.isNotEmpty(comment.getFirstCommentUid())) {
            //删除的是子评论
            commentUid = comment.getFirstCommentUid();
        } else {
            //删除一级评论
            commentUid = comment.getUid();
        }
        // 获取该评论一级评论下所有的子评论re
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("first_comment_uid", commentUid);
        queryWrapper.eq("status", EStatus.ENABLE);
        List<Comment> toCommentList = commentMapper.selectList(queryWrapper);
        List<Comment> resultList = new ArrayList<>();
        this.getToCommentList(comment, toCommentList, resultList);

        // 将所有的子评论也删除
        if (resultList.size() > 0) {
            resultList.forEach(item -> {
                item.setStatus(EStatus.DISABLED);
                item.setUpdateTime(new Date());
                commentMapper.updateById(item);
            });

//            commentService.updateBatchById(resultList);
        }
        return ResultUtil.result(SysConf.SUCCESS, "删除成功");
    }


    /**
     * 获取评论列表
     *
     * @param commentVO
     * @return
     */

    @Override
    public String getList(CommentVO commentVO) {

        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(commentVO.getBlogUid())) {
            queryWrapper.eq("blog_uid", commentVO.getBlogUid());
        }
        queryWrapper.eq("source", commentVO.getSource());
        //分页查询
        Page<Comment> page = new Page<>();
        page.setCurrent(commentVO.getCurrentPage());//当前页
        page.setSize(commentVO.getPageSize());//查询页面数目
        queryWrapper.eq("status", EStatus.ENABLE);
        queryWrapper.isNull("to_uid");
        queryWrapper.orderByDesc("create_time");
        queryWrapper.eq("TYPE", 0);
        //查询所有的一级评论，进行分页展示
        IPage<Comment> pageList = commentMapper.selectPage(page, queryWrapper);
        List<Comment> list = pageList.getRecords();
        List<String> firsuidList = new ArrayList<>();
        list.forEach(item -> {
            firsuidList.add(item.getUid());
        });

//       查询一级评论下的子评论
        if (firsuidList.size() > 0) {
            QueryWrapper<Comment> notQuerWrapper = new QueryWrapper<>();
            notQuerWrapper.in("first_comment_uid", firsuidList);
            notQuerWrapper.eq("status", EStatus.ENABLE);
            List<Comment> notFirstList = commentMapper.selectList(notQuerWrapper);
            //将子评论加到总评论当中
            if (notFirstList.size() > 0) {
                list.addAll(notFirstList);
            }
        }
        List<String> userUidList = new ArrayList<>();
        list.forEach(item -> {
            String userid = item.getUserUid();
            String touseruid = item.getToUserUid();
            if (StringUtils.isNotEmpty(userid)) {
                userUidList.add(item.getUserUid());//用户UID
            }
            if (StringUtils.isNotEmpty(touseruid)) {
                userUidList.add(item.getToUserUid());//回复某个人的UID
            }
        });

        Collection<User> userList = new ArrayList<>();
        if (userUidList.size() > 0) {
            userList = userMapper.selectBatchIds(userUidList);
        }

        // 过滤掉用户的敏感信息
        List<User> filterUserList = new ArrayList<>();
        userList.forEach(item -> {
            User user = new User();
            user.setAvatar(item.getAvatar());
            user.setUid(item.getUid());
            user.setNickName(item.getNickName());
            user.setUserTag(item.getUserTag());
            filterUserList.add(user);
        });
        /*获取用户头像*/
        StringBuffer fileUids = new StringBuffer();
        filterUserList.forEach(user1 -> {
            if (StringUtils.isNotEmpty(user1.getAvatar())) {
                fileUids.append(user1.getAvatar() + SysConf.FILE_SEGMENTATION);
            }
        });
        Map<String, String> pictrueMap = new HashMap<>();
        Map<String, User> userMap = new HashMap<>();
        filterUserList.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getAvatar()) && pictrueMap.get(item.getAvatar()) != null) {
                item.setPhotoUrl(pictrueMap.get(item.getAvatar()));
            }
            userMap.put(item.getUid(), item);
        });
        Map<String, Comment> commentMap = new HashMap<>();
        list.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getUserUid())) {
                item.setUser(userMap.get(item.getUserUid()));
            }
            if (StringUtils.isNotEmpty(item.getToUserUid())) {
                item.setToUser(userMap.get(item.getToUserUid()));
            }
            commentMap.put(item.getUid(), item);
        });
        //设置一级评论下的子评论
        Map<String, List<Comment>> toCommentListMap = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            List<Comment> tempList = new ArrayList<>();
            for (int j = 0; j < list.size(); j++) {
                if (list.get(i).getUid().equals(list.get(j).getToUid())) {
                    tempList.add(list.get(j));
                }
            }
            toCommentListMap.put(list.get(i).getUid(), tempList);
        }
        List<Comment> firstComment = new ArrayList<>();
        list.forEach(item -> {
            if (StringUtils.isEmpty(item.getToUid())) {
                firstComment.add(item);
            }
        });
        pageList.setRecords(getCommentReplys(firstComment, toCommentListMap));
        return ResultUtil.result(SysConf.SUCCESS, pageList);
    }

    /**
     * 获取用户点赞数
     *
     * @param
     * @return
     */
    @Override
    public String getPraiseListByUser(Long currentPage, Long pageSize) {
        ServletRequestAttributes servletRequestAttributes =
                (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();

        if (request.getAttribute(SysConf.USER_UID) == null) {
            return ResultUtil.result(SysConf.ERROR, "未识别改用户！");
        }

        String userUid = request.getAttribute(SysConf.USER_UID).toString();
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_uid", userUid);
        queryWrapper.eq("type", 1);
        queryWrapper.eq("status", EStatus.ENABLE);
        queryWrapper.orderByDesc("create_time");
        Page<Comment> commentPage = new Page<>();
        commentPage.setCurrent(currentPage);
        commentPage.setSize(pageSize);
        Page<Comment> page = commentMapper.selectPage(commentPage, queryWrapper);
        List<Comment> praiseList = page.getRecords();
        List<String> blogUids = new ArrayList<>();
        praiseList.forEach(item -> {
            blogUids.add(item.getBlogUid());
        });
        Map<String, Blog> blogMap = new HashMap<>();
        if (blogUids.size() > 0) {
            Collection<Blog> blogList = blogMapper.selectBatchIds(blogUids);
            blogList.forEach(blog -> {
                // 并不需要content内容
                blog.setContent("");
                blogMap.put(blog.getUid(), blog);
            });
        }
        praiseList.forEach(item -> {
            if (blogMap.get(item.getBlogUid()) != null) {
                item.setBlog(blogMap.get(item.getBlogUid()));
            }
        });
        page.setRecords(praiseList);
        return ResultUtil.result(SysConf.SUCCESS, commentPage);
    }

    /**
     * 获取用户收藏的博客
     *
     * @param currentPage
     * @param pageSize
     * @return
     */
    @Override
    public String getCollectListByUser(Long currentPage, Long pageSize) {
        ServletRequestAttributes servletRequestAttributes =
                (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();

        if (request.getAttribute(SysConf.USER_UID) == null) {
            return ResultUtil.result(SysConf.ERROR, "未识别改用户！");
        }

        //获取用户id
        String userUid = request.getAttribute(SysConf.USER_UID).toString();
        QueryWrapper<Collect> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_uid", userUid);
        queryWrapper.eq("status", EStatus.ENABLE);
        queryWrapper.orderByDesc("create_time");
        List<Collect> collectList = contentMapper.selectList(queryWrapper);
        //查询出所有的收藏博客ID
        List<String> blogUids = new ArrayList<>();
        for (Collect collect : collectList) {
            blogUids.add(collect.getBlogUid());
        }
        //查询出所有收藏的博客
        List<Blog> blogs = new ArrayList<>();
        for (String blogUid : blogUids) {
            Blog blog = blogMapper.selectById(blogUid);
            //不需要获取内容
            blog.setContent(" ");
            blogs.add(blog);
        }
        //分页
        Page<Blog> blogPage = new Page<>();
        blogPage.setCurrent(currentPage);
        blogPage.setSize(pageSize);
        blogPage.setRecords(blogs);

        return ResultUtil.result(SysConf.SUCCESS, blogPage);
    }

    /**
     * 获取评论所有回复
     *
     * @param list
     * @param toCommentListMap
     * @return
     */
    private List<Comment> getCommentReplys(List<Comment> list, Map<String, List<Comment>> toCommentListMap) {
        if (list == null || list.size() == 0) {
            return new ArrayList<>();
        } else {
            list.forEach(item -> {
                String commentUid = item.getUid();
                List<Comment> replyCommentList = toCommentListMap.get(commentUid);
                List<Comment> replyComments = getCommentReplys(replyCommentList, toCommentListMap);
                item.setReplyList(replyComments);
            });
            return list;
        }
    }

    /**
     * 获取某条评论下的所有子评论
     *
     * @return
     */
    private void getToCommentList(Comment comment, List<Comment> commentList, List<Comment> resultList) {
        if (comment == null) {
            return;
        }
        String commentUid = comment.getUid();
        for (Comment item : commentList) {
            if (commentUid.equals(item.getToUid())) {
                resultList.add(item);
                // 寻找子评论的子评论
                getToCommentList(item, commentList, resultList);
            }
        }
    }
}
