package com.jl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jl.base.EPublish;
import com.jl.mapper.BlogMapper;
import com.jl.pojo.Blog;
import com.jl.service.SortService;
import com.jl.utils.EStatus;
import com.jl.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class SortServiceImpl implements SortService {
    @Autowired
    private BlogMapper blogMapper;


    @Override
    public Object getArticleByMonth(String monthDate) {
      // 第一次启动的时候归档
      QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
      queryWrapper.eq("status", EStatus.ENABLE);
      queryWrapper.orderByDesc("create_time");
      queryWrapper.eq("is_publish", EPublish.PUBLISH);
      //因为首页并不需要显示内容，所以需要排除掉内容字段
      queryWrapper.select(Blog.class, i -> !i.getProperty().equals("content"));
      List<Blog> list = blogMapper.selectList(queryWrapper);

      Map<String, List<Blog>> map = new HashMap<>();
      for (Blog blog : list) {
        Date createTime = blog.getCreateTime();

        String month = new SimpleDateFormat("yyyy年MM月").format(createTime).toString();

        if (map.get(month) == null) {
          List<Blog> blogList = new ArrayList<>();
          blogList.add(blog);
          map.put(month, blogList);
        } else {
          List<Blog> blogList = map.get(month);
          blogList.add(blog);
          map.put(month, blogList);
        }
      }
      return map.get(monthDate);
    }

    @Override
    public Object getSortList() {

      // 第一次启动的时候归档
      QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
      queryWrapper.eq("status", EStatus.ENABLE);
      queryWrapper.orderByDesc("create_time");
      queryWrapper.eq("is_publish", EPublish.PUBLISH);
      //因为首页并不需要显示内容，所以需要排除掉内容字段
      queryWrapper.select(Blog.class, i -> !i.getProperty().equals("content"));
      List<Blog> list = blogMapper.selectList(queryWrapper);

      Iterator iterable = list.iterator();
      Set<String> monthSet = new TreeSet<>();
      while (iterable.hasNext()) {
        Blog blog = (Blog) iterable.next();
        Date createTime = blog.getCreateTime();

        String month = new SimpleDateFormat("yyyy年MM月").format(createTime).toString();

        monthSet.add(month);
      }
     return monthSet;
    }
}
