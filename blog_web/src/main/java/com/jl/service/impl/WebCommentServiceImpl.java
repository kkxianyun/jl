package com.jl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jl.mapper.UserMapper;
import com.jl.mapper.WebCommentMapper;
import com.jl.pojo.Comment;
import com.jl.pojo.User;
import com.jl.service.WebCommentService;
import com.jl.utils.ResultUtil;
import com.jl.utils.StringUtils;
import com.jl.utils.SysConf;
import com.jl.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WebCommentServiceImpl implements WebCommentService {

    @Autowired
    private WebCommentMapper webCommentMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 获取用户的评论列表和回复
     *
     * @param request
     * @param pageResult
     * @return
     */
    @Override
    public String getListByUser(HttpServletRequest request, PageResult pageResult) {

        if (request.getAttribute(SysConf.USER_UID) == null) {
            return ResultUtil.errorWithMessage("请先登录！");
        }

        String userUid = (String) request.getAttribute(SysConf.USER_UID);

        // 获取用户的评论
        QueryWrapper<Comment> commentQueryWrapper = new QueryWrapper<>();
        commentQueryWrapper.eq("user_uid", userUid);
        commentQueryWrapper.eq("status", 1);
        commentQueryWrapper.eq("type", 0);
        commentQueryWrapper.orderByDesc("create_time");

        Integer currentPage = pageResult.getCurrentPage();
        Integer pageSize = pageResult.getPageSize();
        Page<Comment> commentPage = new Page<>(currentPage, pageSize);
        Page<Comment> commentPageResult = webCommentMapper.selectPage(commentPage, commentQueryWrapper);

        // “我的评论”列表
        List<Comment> commentList = commentPageResult.getRecords();

        // 查询并设置评论的user和toUser
        if (commentList != null && commentList.size() != 0) {
            commentList = setUserAndToUser(commentList);
        }

        // 获取用户的回复
        QueryWrapper<Comment> replyQueryWrapper = new QueryWrapper<>();
        replyQueryWrapper.eq("to_user_uid", userUid);
        replyQueryWrapper.eq("status", 1);
        replyQueryWrapper.eq("type", 0);
        replyQueryWrapper.orderByDesc("create_time");

        Page<Comment> replyPageResult = webCommentMapper.selectPage(commentPage, replyQueryWrapper);
        // “我的回复”列表
        List<Comment> replyList = replyPageResult.getRecords();

        // 查询并设置回复列表的user和toUser
        if (replyList != null && replyList.size() != 0) {
            replyList = setUserAndToUser(replyList);
        }

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put(SysConf.COMMENT_LIST, commentList);
        resultMap.put(SysConf.REPLY_LIST, replyList);
        return ResultUtil.result(SysConf.SUCCESS, resultMap);
    }

    private List<Comment> setUserAndToUser(List<Comment> commentList) {
        for (Comment comment : commentList) {
            // 设置user
            User user = new User();
            User user1 = userMapper.selectById(comment.getUserUid());
            if (user1 != null) {
                user.setUid(user1.getUid());
                user.setNickName(user1.getNickName());
                user.setAvatar(user1.getAvatar());
            }
            comment.setUser(user);

            // 设置toUser
            User toUser = new User();
            User user2 = userMapper.selectById(comment.getToUserUid());
            if (user2 != null) {
                toUser.setAvatar(user2.getAvatar());
                toUser.setNickName(user2.getNickName());
                toUser.setUid(user2.getUid());
            }
            comment.setToUser(toUser);
        }
        return commentList;
    }

    /**
     * No.6 获取用户收到的评论回复数
     */
    @Override
    public String getUserReceiveCommentCount(HttpServletRequest request) {

        // 判断用户是否登录
        Integer commentCount = 0;
        if (request.getAttribute(SysConf.USER_UID) != null) {
            String userUid = request.getAttribute(SysConf.USER_UID).toString();
            String redisKey = "userReceiveCommentCount:" + userUid;
            String count = stringRedisTemplate.opsForValue().get(redisKey);
            if (StringUtils.isNotEmpty(count)) {
                commentCount = Integer.valueOf(count);
            }
        }
        return ResultUtil.successWithData(commentCount);
    }


    /**
     * No.7 阅读用户接收的评论数
     *
     * @param
     * @Author LiuKun
     */
    @Override
    public String readUserReceiveCommentCount(HttpServletRequest request) {
        // 判断用户是否登录
        if (request.getAttribute(SysConf.USER_UID) != null) {
            String userUid = request.getAttribute(SysConf.USER_UID).toString();
            String redisKey = "userReceiveCommentCount:" + userUid;
            stringRedisTemplate.delete(redisKey);
        }
        return ResultUtil.successWithMessage("阅读成功");
    }

}
