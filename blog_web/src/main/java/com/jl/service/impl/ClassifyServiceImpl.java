package com.jl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jl.mapper.BlogMapper;
import com.jl.mapper.BlogSortMapper;
import com.jl.pojo.Blog;
import com.jl.pojo.BlogSort;
import com.jl.service.ClassifyService;
import com.jl.utils.ResultUtil;
import com.jl.utils.SysConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 博客分类Service实现类
 *
 * @author Lanjie
 * @date 2022-03-03 10:37
 */
@Service
public class ClassifyServiceImpl implements ClassifyService {

    @Autowired
    private BlogSortMapper blogSortMapper;

    @Autowired
    private BlogMapper blogMapper;

    @Override
    public List<BlogSort> getBlogSortList() {

        return blogSortMapper.selectList(null);
    }

    @Override
    public IPage<Blog> getListByBlogSortUid(String blogSortUid, Long currentPage, Long pageSize) {
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("blog_sort_uid", blogSortUid);
        currentPage = currentPage == null ? 1 : currentPage;
        pageSize = pageSize == null? 10 : pageSize;
        Page<Blog> page = new Page<>(currentPage, pageSize);

        return blogMapper.selectPage(page, queryWrapper);
    }
}
