package com.jl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jl.mapper.WebConfigMapper;
import com.jl.pojo.WebConfig;
import com.jl.service.WebConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WebConfigServiceImpl implements WebConfigService {

    @Autowired
    private WebConfigMapper webConfigMapper;

    @Override
    public WebConfig getWebConfigList() {
        QueryWrapper<WebConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        WebConfig webConfig = webConfigMapper.selectOne(queryWrapper);
        return webConfig;
    }
}
