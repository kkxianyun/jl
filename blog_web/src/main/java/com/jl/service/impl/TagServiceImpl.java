package com.jl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jl.base.EPublish;
import com.jl.mapper.BlogMapper;
import com.jl.mapper.TagMapper;
import com.jl.pojo.Blog;
import com.jl.pojo.Tag;
import com.jl.service.TagService;
import com.jl.utils.EStatus;
import com.jl.utils.SysConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class TagServiceImpl implements TagService {
    @Autowired
    private BlogMapper blogMapper;
    @Autowired
    private TagMapper tagMapper;

    @Override
    public Object getArticleByTagUid(String tagUid, Long currentPage, Long pageSize) {

        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        Page<Blog> page = new Page<>();
        page.setCurrent(0);
        page.setSize(10);

        queryWrapper.like("tag_uid", tagUid);
        queryWrapper.eq("status", EStatus.ENABLE);
        queryWrapper.eq("is_publish", EPublish.PUBLISH);
        queryWrapper.orderByDesc("create_time");
        queryWrapper.select(Blog.class, i -> !i.getProperty().equals("content"));
        return blogMapper.selectPage(page, queryWrapper);
    }

    @Override
    public Object getTagList() {
        QueryWrapper<Tag> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SysConf.STATUS, EStatus.ENABLE);
        queryWrapper.orderByDesc("sort");
        return tagMapper.selectList(queryWrapper);
    }
}
