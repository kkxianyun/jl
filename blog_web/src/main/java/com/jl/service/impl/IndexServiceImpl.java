package com.jl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jl.base.EPublish;
import com.jl.mapper.BlogMapper;
import com.jl.mapper.PictureMapper;
import com.jl.mapper.TagMapper;
import com.jl.mapper.webNavbarMapper;
import com.jl.pojo.Blog;
import com.jl.pojo.Picture;
import com.jl.pojo.Tag;
import com.jl.pojo.WebNavbar;
import com.jl.service.IndexService;
import com.jl.utils.EStatus;
import com.jl.utils.StringUtils;
import com.jl.utils.SysConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public class IndexServiceImpl implements IndexService {

    @Autowired
    private webNavbarMapper webNavbarMapper;

    @Autowired
    private BlogMapper blogMapper;

    @Autowired
    private TagMapper tagMapper;

    @Autowired
    private PictureMapper pictureMapper;

    @Override
    public List<WebNavbar> getAllList() {
        QueryWrapper<WebNavbar> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("navbar_level", "1");
        queryWrapper.orderByDesc("sort");
        queryWrapper.eq("status", EStatus.ENABLE);

        List<WebNavbar> list = webNavbarMapper.selectList(queryWrapper);
        //获取所有的ID，去寻找他的子目录
        List<String> ids = new ArrayList<>();
        list.forEach(item -> {
            if (!StringUtils.isEmpty(item.getUid())) {
                ids.add(item.getUid());
            }
        });
        QueryWrapper<WebNavbar> childWrapper = new QueryWrapper<>();
        childWrapper.in("parent_uid", ids);
        childWrapper.eq("status", EStatus.ENABLE);
        Collection<WebNavbar> childList = webNavbarMapper.selectList(childWrapper);

        // 给一级导航栏设置二级导航栏
        for (WebNavbar parentItem : list) {
            List<WebNavbar> tempList = new ArrayList<>();
            for (WebNavbar item : childList) {
                if (item.getParentUid().equals(parentItem.getUid())) {
                    tempList.add(item);
                }
            }
            Collections.sort(tempList);
            parentItem.setChildWebNavbar(tempList);
        }
        return list;
    }

    /**
     * 获取当前最热博客，按点击量排序
     *
     * @return
     */
    @Override
    public IPage<Blog> getHotBlog() {
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();

        // 满足enable状态和已发布
        queryWrapper.eq("status", EStatus.ENABLE);
        queryWrapper.eq("is_publish", EPublish.PUBLISH);

        // 按点击量倒序排序
        queryWrapper.orderByDesc("click_count");

        // 取首页6条返回
        Page<Blog> page = new Page<>(0, 6);
        Page<Blog> blogPage = blogMapper.selectPage(page, queryWrapper);
        List<Blog> blogList = blogPage.getRecords();

        // 设置标题图
        blogList = setBlogPhotoList(blogList);
        blogPage.setRecords(blogList);
        return blogPage;
    }

    // 设置标题图
    private List<Blog> setBlogPhotoList(List<Blog> blogList) {
        if (blogList != null && blogList.size() != 0) {
            for (Blog blog : blogList) {
                List<String> photoList = new ArrayList<>();
                String fileUid = blog.getFileUid();
                if (StringUtils.isNotEmpty(fileUid)) {
                    String[] fileUids = fileUid.split(",");
                    for (String uid : fileUids) {
                        QueryWrapper<Picture> pictureQueryWrapper = new QueryWrapper<>();
                        pictureQueryWrapper.eq("file_uid", fileUid);
                        Picture picture = pictureMapper.selectOne(pictureQueryWrapper);
                        if (picture != null) {
                            photoList.add(picture.getPicName());
                        }
                    }
                }
                blog.setPhotoList(photoList);
                blog.setContent(null);
            }
        }
        return blogList;
    }

    @Override
    public Object getNewBloglist(long currentPage, long pageSize) {
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", EStatus.ENABLE)
                .eq("is_publish", EPublish.PUBLISH)
                .orderByDesc("create_time");
        //分页
        Page<Blog> page = new Page<>(currentPage = currentPage, pageSize = pageSize);
        Page<Blog> blogPage = blogMapper.selectPage(page, queryWrapper);

        List<Blog> blogList = blogPage.getRecords();

        // 设置标题图
        blogList = setBlogPhotoList(blogList);
        blogPage.setRecords(blogList);
        return blogPage;
    }

    @Override
    public Object getBlogBySearch(String keywordslong, long currentPage, long pageSize) {
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        boolean flag = StringUtils.isEmpty(keywordslong);
        queryWrapper.like(flag, "content", keywordslong)
                .eq("status", EStatus.ENABLE)
                .eq("is_publish", EPublish.PUBLISH);
        Page<Blog> page = new Page<>(currentPage = 1, pageSize = 3);
        return blogMapper.selectPage(page, queryWrapper);
    }

    @Override
    public Object getHotTag() {
        QueryWrapper<Tag> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SysConf.STATUS, EStatus.ENABLE);
        queryWrapper.orderByDesc("click_count");
        return tagMapper.selectList(queryWrapper);
    }
}
