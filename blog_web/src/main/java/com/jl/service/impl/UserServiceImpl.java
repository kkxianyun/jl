package com.jl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jl.mapper.UserMapper;
import com.jl.pojo.User;
import com.jl.service.UserService;
import com.jl.utils.EStatus;
import com.jl.utils.MD5Utils;
import com.jl.utils.ResultUtil;
import com.jl.utils.SysConf;
import com.jl.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/***
 *
 *   █████▒█    ██  ▄████▄   ██ ▄█▀       ██████╗ ██╗   ██╗ ██████╗
 * ▓██   ▒ ██  ▓██▒▒██▀ ▀█   ██▄█▒        ██╔══██╗██║   ██║██╔════╝
 * ▒████ ░▓██  ▒██░▒▓█    ▄ ▓███▄░        ██████╔╝██║   ██║██║  ███╗
 * ░▓█▒  ░▓▓█  ░██░▒▓▓▄ ▄██▒▓██ █▄        ██╔══██╗██║   ██║██║   ██║
 * ░▒█░   ▒▒█████▓ ▒ ▓███▀ ░▒██▒ █▄       ██████╔╝╚██████╔╝╚██████╔╝
 *  ▒ ░   ░▒▓▒ ▒ ▒ ░ ░▒ ▒  ░▒ ▒▒ ▓▒       ╚═════╝  ╚═════╝  ╚═════╝
 *  ░     ░░▒░ ░ ░   ░  ▒   ░ ░▒ ▒░
 *  ░ ░    ░░░ ░ ░ ░        ░ ░░ ░
 *           ░     ░ ░      ░  ░
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public String updateUserPwd(HttpServletRequest request, String oldPwd, String newPwd) {
        //判断用户密码是否为空
        if (oldPwd == null || oldPwd.length() <= 0) {
            return ResultUtil.result(SysConf.ERROR, "传入参数有误");
        }
        if (request.getAttribute(SysConf.USER_UID) == null) {
            return ResultUtil.result(SysConf.ERROR, "Token未被识别");
        }
        String userUid = request.getAttribute(SysConf.USER_UID).toString();
        User user = userMapper.selectById(userUid);
        // 判断旧密码是否一致
        if (user.getPassWord().equals(MD5Utils.string2MD5(oldPwd))) {
            user.setPassWord(MD5Utils.string2MD5(newPwd));
            //更新修改时间
            user.setUpdateTime(new Date());
            user.updateById();
            return ResultUtil.result(SysConf.SUCCESS, "操作成功");
        }
        return ResultUtil.result(SysConf.ERROR, "输入密码有误");
    }

    @Override
    public User getUserByUid(String useruid) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("uid", useruid);
        User user = userMapper.selectOne(queryWrapper);
        return user;
    }

    @Override
    public String findUserByNameAndPwd(UserVO userVO) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_name", userVO.getUserName());
        String password = MD5Utils.string2MD5(userVO.getPassWord());
        queryWrapper.eq("pass_word", password);
        User user = userMapper.selectOne(queryWrapper);
        if (user == null) {
            return ResultUtil.errorWithData("账号或密码错误！");
        }
        if (user.getStatus() == EStatus.DISABLED) {
            return ResultUtil.errorWithData("该账号已被冻结！");
        }
        if (EStatus.FREEZE == user.getStatus()) {
            return ResultUtil.result(SysConf.ERROR, "用户账号未激活");
        }
        user.setLastLoginTime(new Date());
//        String token = StringUtils.getUUID();
        String token = user.getUid() + " " + user.getUserName();

        // 将token存入redis
        String redisKey = "token:" + user.getUid();
        stringRedisTemplate.opsForValue().set(redisKey, token, 1, TimeUnit.DAYS);

        return ResultUtil.successWithData(token);
    }

    @Override
    public String register(UserVO userVO) {
        User user = new User();
        user.setUserName(userVO.getUserName());
        user.setPassWord(MD5Utils.string2MD5(userVO.getPassWord()));
        user.setNickName(userVO.getNickName());
        user.setEmail(userVO.getEmail());
        user.setSource("blog");
        user.setStatus(EStatus.ENABLE);
        int insert = userMapper.insert(user);
        if (insert != 1)
            return ResultUtil.errorWithData("注册失败！");
        return ResultUtil.successWithData("注册成功！");
    }

    @Override
    public String editUser(UserVO userVO) {
        User user = userMapper.selectById(userVO.getUid());
        if (user != null) {
            user.setNickName(userVO.getNickName());
            user.setAvatar(userVO.getAvatar());
            user.setBirthday(userVO.getBirthday());
            user.setSummary(userVO.getSummary());
            user.setGender(userVO.getGender());
            user.setQqNumber(userVO.getQqNumber());
            user.setOccupation(userVO.getOccupation());
            userMapper.updateById(user);
            return ResultUtil.successWithData("用户更新成功！");
        }
        return ResultUtil.errorWithData("编辑失败, 未找到该用户!");
    }
}
