package com.jl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jl.base.EPublish;
import com.jl.mapper.BlogMapper;
import com.jl.mapper.BlogSortMapper;
import com.jl.mapper.TagMapper;
import com.jl.pojo.Blog;
import com.jl.pojo.BlogSort;
import com.jl.pojo.Tag;
import com.jl.service.SearchService;
import com.jl.utils.EStatus;
import com.jl.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * @Author YanYong
 * @Date 2022/3/3 17:13
 */
@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    private BlogMapper blogMapper;

    @Autowired
    private TagMapper tagMapper;

    @Autowired
    private BlogSortMapper blogSortMapper;

    @Override
    public Object sqlSearchBlog(String keywords, long currentPage, long pageSize) {
        final String keyword = keywords.trim();
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.and(new Consumer<QueryWrapper<Blog>>() {
            @Override
            public void accept(QueryWrapper<Blog> wrapper) {
                wrapper.like("title", keyword).or().like("summary", keyword);
            }
        });
        queryWrapper.eq("status", EStatus.ENABLE);
        queryWrapper.eq("is_publish", EPublish.PUBLISH);
        queryWrapper.select(Blog.class, i -> !i.getProperty().equals("content"));
        queryWrapper.orderByDesc("click_count");
        Page<Blog> page = new Page<>();
        page.setCurrent(currentPage);
        page.setSize(pageSize);

        Page<Blog> blogPage = blogMapper.selectPage(page, queryWrapper);
        List<Blog> blogList = blogPage.getRecords();
        List<String> blogSortUidList = new ArrayList<>();
        Map<String, String> pictureMap = new HashMap<>();
        final StringBuffer fileUids = new StringBuffer();
        blogList.forEach(item -> {
            // 获取图片uid
            blogSortUidList.add(item.getBlogSortUid());
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                fileUids.append(item.getFileUid() + ",");
            }
            // 给标题和简介设置高亮
            item.setTitle(getHitCode(item.getTitle(), keyword));
            item.setSummary(getHitCode(item.getSummary(), keyword));

        });

        Map<String, Object> map = new HashMap<>();
        // 返回总记录数
        map.put("total", blogPage.getTotal());
        // 返回总页数
        map.put("totalPage", blogPage.getPages());
        // 返回当前页大小
        map.put("pageSize", pageSize);
        // 返回当前页
        map.put("currentPage", blogPage.getCurrent());
        // 返回数据
        map.put("blogList", blogList);
        return map;
    }

    /**
     * 添加高亮
     *
     * @param str
     * @param keyword
     * @return
     */
    private String getHitCode(String str, String keyword) {
        if (StringUtils.isEmpty(keyword) || StringUtils.isEmpty(str)) {
            return str;
        }
        String startStr = "<span style = 'color:red'>";
        String endStr = "</span>";
        // 判断关键字是否直接是搜索的内容，否者直接返回
        if (str.equals(keyword)) {
            return startStr + str + endStr;
        }
        String lowerCaseStr = str.toLowerCase();
        String lowerKeyword = keyword.toLowerCase();
        String[] lowerCaseArray = lowerCaseStr.split(lowerKeyword);
        Boolean isEndWith = lowerCaseStr.endsWith(lowerKeyword);

        // 计算分割后的字符串位置
        Integer count = 0;
        List<Map<String, Integer>> list = new ArrayList<>();
        List<Map<String, Integer>> keyList = new ArrayList<>();
        for (int a = 0; a < lowerCaseArray.length; a++) {
            // 将切割出来的存储map
            Map<String, Integer> map = new HashMap<>();
            Map<String, Integer> keyMap = new HashMap<>();
            map.put("startIndex", count);
            Integer len = lowerCaseArray[a].length();
            count += len;
            map.put("endIndex", count);
            list.add(map);
            if (a < lowerCaseArray.length - 1 || isEndWith) {
                // 将keyword存储map
                keyMap.put("startIndex", count);
                count += keyword.length();
                keyMap.put("endIndex", count);
                keyList.add(keyMap);
            }
        }
        // 截取切割对象
        List<String> arrayList = new ArrayList<>();
        for (Map<String, Integer> item : list) {
            Integer start = item.get("startIndex");
            Integer end = item.get("endIndex");
            String itemStr = str.substring(start, end);
            arrayList.add(itemStr);
        }
        // 截取关键字
        List<String> keyArrayList = new ArrayList<>();
        for (Map<String, Integer> item : keyList) {
            Integer start = item.get("startIndex");
            Integer end = item.get("endIndex");
            String itemStr = str.substring(start, end);
            keyArrayList.add(itemStr);
        }

        StringBuffer sb = new StringBuffer();
        for (int a = 0; a < arrayList.size(); a++) {
            sb.append(arrayList.get(a));
            if (a < arrayList.size() - 1 || isEndWith) {
                sb.append(startStr);
                sb.append(keyArrayList.get(a));
                sb.append(endStr);
            }
        }
        return sb.toString();
    }

    @Override
    public Object searchBlogByTagUid(String tagUid, Integer currentPage, Integer pageSize) {
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("tag_uid", tagUid);
        queryWrapper.eq("status", 1);
        queryWrapper.eq("is_publish", "1");
        queryWrapper.eq("type", 0);

        Page<Blog> page = new Page<>(currentPage, pageSize);
        Page<Blog> blogPage = blogMapper.selectPage(page, queryWrapper);
        List<Blog> blogList = blogPage.getRecords();

        if (blogList != null && blogList.size() != 0) {

            for (Blog blog : blogList) {

                // 设置标签
                String tagUids = blog.getTagUid();
                ArrayList<Tag> tagList = new ArrayList<>();
                if (StringUtils.isNotEmpty(tagUids)) {
                    String[] uids = tagUids.split(",");
                    for (String uid : uids) {
                        QueryWrapper<Tag> tagQueryWrapper = new QueryWrapper<>();
                        queryWrapper.eq("uid", uid);
                        Tag tag = tagMapper.selectById(uid);
                        tagList.add(tag);
                    }
                }
                blog.setTagList(tagList);

                // 设置分类
                String blogSortUid = blog.getBlogSortUid();
                if (StringUtils.isNotEmpty(blogSortUid)) {
                    BlogSort blogSort = blogSortMapper.selectById(blogSortUid);
                    blog.setBlogSort(blogSort);
                }
            }
        }

        blogPage.setRecords(blogList);
        return blogPage;
    }
}
