package com.jl.service.impl;

import com.google.gson.Gson;
import com.jl.mapper.PictureMapper;
import com.jl.pojo.Picture;
import com.jl.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Author YanYong
 * @Date 2022/3/9 23:52
 */
@Service
public class FileServiceImpl implements FileService {

    @Autowired
    private PictureMapper pictureMapper;

    /**
     * 定义目录
     */
    private String preFilePath = "D:/blogImages";

    /**
     * 定义网络地址的前缀
     */
    private String preURLPath = "http://localhost:8091/image";

    /**
     * 图片上传：
     * 1.校验图片的类型 jpg/png/gif
     * 2.校验是否为恶意程序  木马.exe.jpg
     * 3.将图片分目录存储  hash方式存储/时间格式存储
     * 4.防止图片重名，使用UUID。
     *
     * @param file
     * @return
     */
    @Override
    public List<String> upload(MultipartFile file) {

        // 校验图片名称格式
        String filename = file.getOriginalFilename().toLowerCase();
        if (!filename.matches("^.+\\.(jpg|png|gif)$")) {

            // 如果图片不满足条件，则程序终止
            return null;
        }

        // 第二步：校验是否为恶意程序
        try {
            BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
            int height = bufferedImage.getHeight();
            int width = bufferedImage.getWidth();
            if (height == 0 || width == 0) {
                return null;
            }

            // 第三步：分目录存储  提高检索的效率  按照时间将目录划分  /yyyy/MM/dd/
            String datePath = new SimpleDateFormat("/yyyy/MM/dd/").format(new Date());

            // 文件夹路径 D:/JLBlog/image/2022/03/10
            String fileDir = preFilePath + datePath;

            // 创建目录
            File dirFile = new File(fileDir);
            if (!dirFile.exists()) {
                dirFile.mkdirs();
            }

            // 第四步：防止文件重名
            String uuid = UUID.randomUUID().toString().replace("-", "");
            int index = filename.lastIndexOf(".");

            // 截取文件后缀名 如 .jpg
            String fileType = filename.substring(index);

            // 新文件名
            filename = uuid + fileType;

            //第五步：实现文件上传（即保存到磁盘相应路径）
            String filePath = fileDir + filename;
            file.transferTo(new File(filePath));

            // 第六步：封装ImageVO返回数据
            String virtualPath = datePath + filename;

            // 第七步：封装网络地址http://image.jl.com/2022/03/10/uuid.jpg
            String urlPath = preURLPath + virtualPath;
            System.out.println(urlPath);

            String fileUid = savePicture(urlPath);

            Map<String, String> urlMap = new HashMap<>();
            urlMap.put("url", urlPath);
            String urlGson = new Gson().toJson(urlMap);

            List<String> urlList = new ArrayList<>();
            urlList.add(urlGson);
            return urlList;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将标题图片存入图片表
     *
     * @param urlPath
     * @return
     */
    private String savePicture(String urlPath) {
        String uid = UUID.randomUUID().toString().replace("-", "");
        String fileUid = UUID.randomUUID().toString().replace("-", "");
        Picture picture = new Picture();
        picture.setUid(uid);
        picture.setFileUid(fileUid);
        picture.setPicName(urlPath);
        picture.setCreateTime(new Date());
        picture.setStatus(1);
        picture.setUpdateTime(new Date());

        pictureMapper.savePicture(picture);

        return fileUid;
    }

    @Override
    public void deleteFile(String virtualPath) {
        String filePath = preFilePath + virtualPath;
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
    }
}
