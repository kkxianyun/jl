package com.jl.service;

import com.jl.vo.PageResult;

import javax.servlet.http.HttpServletRequest;

public interface WebCommentService {

    /**
     * 获取用户的评论列表和回复
     *
     * @param request
     * @param pageResult
     * @return
     */
    String getListByUser(HttpServletRequest request, PageResult pageResult);

    /**
     * No.6 获取用户收到的评论回复数
     */
    String getUserReceiveCommentCount(HttpServletRequest request);

    /**
     * No.7 阅读用户接收的评论数
     */
    String readUserReceiveCommentCount(HttpServletRequest request);
}
