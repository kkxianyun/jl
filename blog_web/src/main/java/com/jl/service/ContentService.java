package com.jl.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jl.pojo.Blog;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @ClassName: ContentService
 * @Description:
 * @Date: 2022/2/22 9:37
 * @Author YanYong
 * @Since JDK 1.8
 */
public interface ContentService {


    /**
     * 通过 Uid 获取博客点赞数
     *
     * @param uid
     * @return
     */
    public Integer getBlogPraiseCountByUid(String uid);

    /**
     * 通过 Uid 给博客点赞
     *
     * @param uid
     * @return
     */
    public String praiseBlogByUid(String uid);

    /**
     * 通过uid获取blog
     *
     * @param uid
     * @param oid
     * @return
     */
    String getBlogByUid(String uid, Integer oid);

    /**
     * 根据BlogUid获取相关的博客
     *
     * @param blogUid
     * @return
     */
    Object getSameBlogByBlogUid(String blogUid);

    /**
     * 根据标签Uid获取相关的博客
     *
     * @param tagUid
     * @return
     */
    public IPage<Blog> getSameBlogByTagUid(String tagUid);

    /**
     * 给博客列表设置分类和标签
     *
     * @param list
     * @return
     */
    public List<Blog> setTagAndSortByBlogList(List<Blog> list);

    /**
     * 收藏博客
     *
     * @param uid
     * @return
     */
    String collectBlogByUid(String uid);

    /**
     * 对该博客是否已点赞或收藏
     *
     * @param uid
     * @return
     */
    String isPraiseOrCollect(HttpServletRequest request, String uid);
}
