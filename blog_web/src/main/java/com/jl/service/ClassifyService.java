package com.jl.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jl.pojo.Blog;
import com.jl.pojo.BlogSort;

import java.util.List;

/**
 * 博客分类Service接口
 *
 * @author Lanjie
 * @date 2022-03-03 10:32
 */
public interface ClassifyService {

    List<BlogSort> getBlogSortList();

    IPage<Blog> getListByBlogSortUid(String blogSortUid, Long currentPage, Long pageSize);
}
