package com.jl.controller;

import com.jl.service.CommentService;
import com.jl.vo.CommentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/web/comment")
@CrossOrigin
public class CommentController {

    @Autowired
    private CommentService commentService;

    //增加评论
    @PostMapping("/add")
    public String add(@RequestBody CommentVO commentVO) {

        return commentService.add(commentVO);
    }

    /*
     *删除评论
     */
    @PostMapping("/delete")
    public String delete(@RequestBody CommentVO commentVO) {

        return commentService.delete(commentVO);
    }

    /**
     * 获取评论列表
     */
    @PostMapping("/getList")
    public String getListUsing(@RequestBody CommentVO commentVO) {

        return commentService.getList(commentVO);
    }

    /**
     * 获取用户点赞信息
     */
    @PostMapping("/getPraiseListByUser")
    public String getBlogPraiseCountByUid(@RequestParam(name = "currentPage",required = false,defaultValue = "1") Long currentPage,
                                          @RequestParam(name = "pageSize",required = false,defaultValue = "10") Long pageSize) {
        return commentService.getPraiseListByUser(currentPage, pageSize);
    }

    /**
     * 获取博客收藏表信息
     * @param currentPage
     * @param pageSize
     * @return
     */
    @PostMapping("/getCollectListByUser")
    public String getCollectListByUser(@RequestParam(name = "currentPage",required = false,defaultValue = "1") Long currentPage,
                                       @RequestParam(name = "pageSize",required = false,defaultValue = "10")Long pageSize){
        return commentService.getCollectListByUser(currentPage, pageSize);
    }

}
