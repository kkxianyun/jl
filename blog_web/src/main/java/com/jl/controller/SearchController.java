package com.jl.controller;

import com.jl.service.SearchService;
import com.jl.utils.ResultUtil;
import com.jl.utils.StringUtils;
import com.jl.utils.SysConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author YanYong
 * @Date 2022/3/3 17:12
 */
@CrossOrigin
@RestController
@RequestMapping("/search")
public class SearchController {

    @Autowired
    private SearchService searchService;

    /**
     * 通过sql语句搜索博客内容
     *
     * @param keywords
     * @param currentPage
     * @param pageSize
     * @return
     */
    @GetMapping("/sqlSearchBlog")
    public String sqlSearchBlog(String keywords, long currentPage, long pageSize) {

        if (StringUtils.isEmpty(keywords) || StringUtils.isEmpty(keywords.trim())) {
            return ResultUtil.result(SysConf.ERROR, "关键字不能为空");
        }
        return ResultUtil.result(SysConf.SUCCESS,
                searchService.sqlSearchBlog(keywords, currentPage, pageSize));
    }

    /**
     * 根据标签UID获取博客
     *
     * @param tagUid
     * @param currentPage
     * @param pageSize
     * @return
     */
    @GetMapping("/searchBlogByTag")
    public String searchBlogByTagUid(String tagUid, Integer currentPage, Integer pageSize) {
        return ResultUtil.result(SysConf.SUCCESS,
                searchService.searchBlogByTagUid(tagUid, currentPage, pageSize));
    }
}
