package com.jl.controller;

import com.jl.mapper.BlogMapper;
import com.jl.service.ClassifyService;
import com.jl.utils.ResultUtil;
import com.jl.utils.StringUtils;
import com.jl.utils.SysConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 博客分类Controller
 *
 * @author Lanjie
 * @date 2022-03-03 10:27
 */
@RestController
@CrossOrigin
@RequestMapping("/classify")
public class ClassifyController {

    @Autowired
    private ClassifyService classifyService;

    /**
     * 获取分类的信息
     */
    @GetMapping("/getBlogSortList")
    public String getBlogSortList() {

        return ResultUtil.result(SysConf.SUCCESS, classifyService.getBlogSortList());
    }

    /**
     * 根据分类Uid获取文章
     *
     * @param blogSortUid
     * @param currentPage
     * @param pageSize
     * @return
     */
    @GetMapping("/getArticleByBlogSortUid")
    public String getArticleByBlogSortUid(String blogSortUid, Long currentPage, Long pageSize) {
        if (StringUtils.isBlank(blogSortUid)) {
            return ResultUtil.result(SysConf.ERROR, "blogSortUid不能为空");
        }

        return ResultUtil.result(SysConf.SUCCESS, classifyService.getListByBlogSortUid(blogSortUid, currentPage, pageSize));
    }
}
