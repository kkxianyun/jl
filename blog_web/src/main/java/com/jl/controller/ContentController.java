package com.jl.controller;

import com.jl.service.ContentService;
import com.jl.utils.ResultUtil;
import com.jl.utils.StringUtils;
import com.jl.utils.SysConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName: ContentController
 * @Description:
 * @Date: 2022/2/22 9:35
 * @Author YanYong
 * @Since JDK 1.8
 */
@CrossOrigin(origins = "http://localhost:9527")
@RestController
@RequestMapping("/content")
public class ContentController {

    @Autowired
    private ContentService contentService;

    /**
     * 通过Uid获取博客内容
     *
     * @param uid
     * @param oid
     * @return
     */
    @GetMapping("/getBlogByUid")
    public String getBlogByUid(String uid, Integer oid) {
        if (StringUtils.isEmpty(uid) && oid == 0) {
            return ResultUtil.result(SysConf.ERROR, "传入参数有误！");
        }
        return contentService.getBlogByUid(uid, oid);
    }

    /**
     * 通过 Uid 获取点赞数
     *
     * @param uid
     * @return
     */
    @GetMapping("/getBlogPraiseCountByUid")
    public String getBlogPraiseCountByUid(String uid) {
        return ResultUtil.result(SysConf.SUCCESS, contentService.getBlogPraiseCountByUid(uid));
    }

    /**
     * 通过 Uid 给博客点赞
     *
     * @param uid
     * @return
     */
    @GetMapping("/praiseBlogByUid")
    public String praiseBlogByUid(String uid) {
        if (StringUtils.isEmpty(uid)) {
            return ResultUtil.result(SysConf.ERROR, "传入参数有误！");
        }
        return contentService.praiseBlogByUid(uid);
    }

    /**
     * 根据标签Uid获取相关的博客
     */
    @GetMapping("/getSameBlogByTagUid")
    public String getSameBlogByTagUid(String tagUid, Long currentPage, Long pageSize) {
        if (StringUtils.isEmpty(tagUid)) {
            return ResultUtil.result(SysConf.ERROR, "传入参数有误");
        }
        return ResultUtil.result(SysConf.SUCCESS, contentService.getSameBlogByTagUid(tagUid));
    }

    /**
     * 根据BlogUid获取相关的博客
     * /content/getSameBlogByBlogUid
     */
    @GetMapping("/getSameBlogByBlogUid")
    public String getSameBlogByBlogUid(String blogUid) {
        return ResultUtil.result(SysConf.SUCCESS,
                contentService.getSameBlogByBlogUid(blogUid));
    }

    /**
     * 博客收藏
     *
     * @param uid
     * @return
     */
    @GetMapping("/collectBlogByUid")
    public String collectBlogByUid(String uid) {

        return contentService.collectBlogByUid(uid);
    }

    /**
     * 对该博客是否已点赞或收藏
     *
     * @return
     */
    @GetMapping("/isPraiseOrCollect")
    public String isPraiseOrCollect(HttpServletRequest request, String uid) {
        return contentService.isPraiseOrCollect(request, uid);
    }
}
