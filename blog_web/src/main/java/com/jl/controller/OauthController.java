package com.jl.controller;

import com.jl.service.FeedbackService;
import com.jl.service.UserService;
import com.jl.utils.ResultUtil;
import com.jl.utils.SysConf;
import com.jl.vo.FeedbackVO;
import com.jl.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/***
 *
 *   █████▒█    ██  ▄████▄   ██ ▄█▀       ██████╗ ██╗   ██╗ ██████╗
 * ▓██   ▒ ██  ▓██▒▒██▀ ▀█   ██▄█▒        ██╔══██╗██║   ██║██╔════╝
 * ▒████ ░▓██  ▒██░▒▓█    ▄ ▓███▄░        ██████╔╝██║   ██║██║  ███╗
 * ░▓█▒  ░▓▓█  ░██░▒▓▓▄ ▄██▒▓██ █▄        ██╔══██╗██║   ██║██║   ██║
 * ░▒█░   ▒▒█████▓ ▒ ▓███▀ ░▒██▒ █▄       ██████╔╝╚██████╔╝╚██████╔╝
 *  ▒ ░   ░▒▓▒ ▒ ▒ ░ ░▒ ▒  ░▒ ▒▒ ▓▒       ╚═════╝  ╚═════╝  ╚═════╝
 *  ░     ░░▒░ ░ ░   ░  ▒   ░ ░▒ ▒░
 *  ░ ░    ░░░ ░ ░ ░        ░ ░░ ░
 *           ░     ░ ░      ░  ░
 */
@RestController
@CrossOrigin
@RequestMapping("/oauth")
public class OauthController {

    @Autowired
    private FeedbackService feedbackService;

    @Autowired
    private UserService userService;

    @GetMapping("/getFeedbackList")
    public String getFeedbackList(HttpServletRequest request) {
        return feedbackService.getFeedbackList(request);
    }

    /**
     * 提交反馈
     * @param request
     * @param feedbackVO
     * @return
     */
    @PostMapping("/addFeedback")
    public String addFeedback(HttpServletRequest request, @RequestBody FeedbackVO feedbackVO) {
        return feedbackService.addFeedback(request, feedbackVO);
    }

    /**
     * 更新用户密码
     * @param request
     * @param oldPwd
     * @param newPwd
     * @return
     */
    @PostMapping("/updateUserPwd")
    public String updateUserPwd(HttpServletRequest request, @RequestParam String oldPwd, @RequestParam String newPwd) {
        return userService.updateUserPwd(request, oldPwd, newPwd);
    }

    /**
     * 获取关于我的信息
     */
    @PostMapping("/editUser")
    public String editUser(HttpServletRequest request, @RequestBody UserVO userVO) {
        String userUid = (String) request.getAttribute("userUid");
        if (userUid != null){
            userVO.setUid(userUid);
            return userService.editUser(userVO);
        }
        return ResultUtil.errorWithData("获取信息错误！");
    }
}
