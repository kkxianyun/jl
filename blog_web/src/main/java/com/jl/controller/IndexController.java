package com.jl.controller;

import com.jl.service.IndexService;
import com.jl.service.WebConfigService;
import com.jl.utils.ResultUtil;
import com.jl.utils.SysConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/index")
public class IndexController {

    @Autowired
    private IndexService indexService;

    @Autowired
    private WebConfigService webConfigService;

    @GetMapping("/getWebConfig")
    public String getWebConfig() {
        return ResultUtil.result(SysConf.SUCCESS, webConfigService.getWebConfigList());
    }

    @GetMapping("/getWebNavbar")
    public String getWebNavbar() {
        return ResultUtil.result(SysConf.SUCCESS, indexService.getAllList());
    }

    @GetMapping("/getHotBlog")
    public String getHotBlog() {
        return ResultUtil.result(SysConf.SUCCESS, indexService.getHotBlog());
    }

    @GetMapping("/getNewBlog")
    public String getNewBlog(long currentPage, long pageSize) {
        return ResultUtil.result(SysConf.SUCCESS,
                indexService.getNewBloglist(currentPage, pageSize));
    }

    /**
     * /index/getBlogBySearch
     */
    @GetMapping("/getBlogBySearch")
    public String getBlogBySearch(String keywordslong, long currentPage, long pageSize) {
        return ResultUtil.result(SysConf.SUCCESS,
                indexService.getBlogBySearch(keywordslong, currentPage, pageSize));
    }

    /**
     * 获取博客点击排行
     */
    @GetMapping("/getHotTag")
    public String getHotTag() {
        return ResultUtil.result(SysConf.SUCCESS,
                indexService.getHotTag());
    }
}
