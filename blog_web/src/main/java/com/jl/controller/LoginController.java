package com.jl.controller;

import com.jl.service.UserService;
import com.jl.utils.ResultUtil;
import com.jl.utils.StringUtils;
import com.jl.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public String login(@RequestBody UserVO userVO){
        if (StringUtils.isEmpty(userVO.getUserName()) && StringUtils.isEmpty(userVO.getPassWord()))
            return ResultUtil.errorWithMessage("用户名或密码错误");
        return userService.findUserByNameAndPwd(userVO);
    }

    @PostMapping("/register")
    public String register(@RequestBody UserVO userVO){
        if (StringUtils.isEmpty(userVO.getUserName()) || StringUtils.isEmpty(userVO.getPassWord()))
            throw new RuntimeException("参数错误！");
        return userService.register(userVO);
    }

    public String loginOut(){
        return null;
    }
}
