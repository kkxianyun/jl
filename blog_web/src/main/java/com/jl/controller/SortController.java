package com.jl.controller;

import com.jl.service.SortService;
import com.jl.utils.ResultUtil;
import com.jl.utils.StringUtils;
import com.jl.utils.SysConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @ClassName: ContentController
 * @Description:
 * @Date: 2022/2/22 9:35
 * @Author YanYong
 * @Since JDK 1.8
 */
@CrossOrigin(origins = "http://localhost:9527")
@RestController
@RequestMapping("/sort")
public class SortController {

    @Autowired
    private SortService sortService;

    /**
     * 获取归档月份信息
     * @return
     */
    @GetMapping("/getSortList")
    public String getSortList() {
        return ResultUtil.result(SysConf.SUCCESS, sortService.getSortList());
    }

    /**
     * 通过月份获取文章
     * @return
     */
    @GetMapping("/getArticleByMonth")
    public String getArticleByMonth(String monthDate) {
        if (StringUtils.isEmpty(monthDate)) {
            return ResultUtil.result(SysConf.ERROR, "传入参数有误！");
        }

        return ResultUtil.result(SysConf.SUCCESS, sortService.getArticleByMonth(monthDate));
    }

}
