package com.jl.controller;

import com.jl.service.TagService;
import com.jl.utils.ResultUtil;
import com.jl.utils.StringUtils;
import com.jl.utils.SysConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
/*
* Zyj 16:29 2022.3.3*/

@RestController
@RequestMapping("/tag")
@CrossOrigin(origins = "http://localhost:9527")
public class TagController {

    @Autowired
    private TagService tagService;
    /**
     * 通过TagUid获取文章
     *
     * @param
     * @param currentPage
     * @param pageSize
     * @return
     */

    @GetMapping("/getArticleByTagUid")
    public String getArticleByTagUid(String tagUid,Long currentPage,Long pageSize) {
        if (StringUtils.isEmpty(tagUid)) {
            return ResultUtil.result(SysConf.ERROR, "传入TagUid不能为空");
        }
        return ResultUtil.result(SysConf.SUCCESS, tagService.getArticleByTagUid(tagUid, currentPage, pageSize));
    }



    /**
     * 获取标签的信息
     *
     * @return
     */
    @GetMapping("/getTagList")
    public String getTagList(){
        return ResultUtil.result(SysConf.SUCCESS, tagService.getTagList());
    }
}
