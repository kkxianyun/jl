package com.jl.controller;

import com.jl.service.WebCommentService;
import com.jl.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin()
@RequestMapping("/web/comment")
public class WebCommentController {

    @Autowired
    private WebCommentService webCommentService;

    /**
     * 获取用户的评论列表和回复
     *
     * @param request
     * @param pageResult
     * @return
     */
    @PostMapping("/getListByUser")
    public String getListByUser(HttpServletRequest request,
                                @RequestBody PageResult pageResult) {
        return webCommentService.getListByUser(request, pageResult);
    }

    /**
     * No.6 获取用户收到的评论回复数
     *
     * @param request
     * @Author LiuKun
     */
    @GetMapping("/getUserReceiveCommentCount")
    public String getUserReceiveCommentCount(HttpServletRequest request) {
        return webCommentService.getUserReceiveCommentCount(request);
    }

    /**
     * No.7 阅读用户接收的评论数
     *
     * @param request
     * @Author LiuKun
     */
    @PostMapping("/readUserReceiveCommentCount")
    public String readUserReceiveCommentCount(HttpServletRequest request) {
        return webCommentService.readUserReceiveCommentCount(request);
    }
}

