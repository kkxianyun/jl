package com.jl.controller;

import com.jl.utils.ResultUtil;
import com.jl.utils.SysConf;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/sysDictData")
public class SysDictDataController {

    @PostMapping("/getListByDictTypeList")
    public String getListByDictTypeList(@RequestBody List<String> dictTypeList) {
        return ResultUtil.result(SysConf.SUCCESS, "sysDictDataService.getListByDictTypeList(dictTypeList)");
    }
}
