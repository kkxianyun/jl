package com.jl.controller;

import com.jl.service.FileService;
import com.jl.utils.ResultUtil;
import com.jl.utils.SysConf;
import com.jl.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Author YanYong
 * @Date 2022/3/9 23:47
 */
@RestController
@CrossOrigin
@RequestMapping("/file")
public class FileController {

    @Autowired
    private FileService fileService;

    /**
     * 文件上传
     *
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public String upload(MultipartFile file) {
        if (file != null) {
            List<String> urlList = fileService.upload(file);
            if (urlList == null) {
                return ResultUtil.errorWithMessage("上传失败！");
            }
            return ResultUtil.result(SysConf.SUCCESS, urlList);
        }
        return ResultUtil.errorWithMessage("上传失败！");
    }

    /**
     * 文件删除
     *
     * @param virtualPath
     * @return
     */
    @DeleteMapping("/deleteFile")
    public SysResult deleteFile(String virtualPath) {
        fileService.deleteFile(virtualPath);
        return SysResult.success();
    }

}
