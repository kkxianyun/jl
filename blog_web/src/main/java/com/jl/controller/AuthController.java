package com.jl.controller;

import com.jl.pojo.User;
import com.jl.service.UserService;
import com.jl.utils.JsonUtils;
import com.jl.utils.ResultUtil;
import com.jl.utils.StringUtils;
import com.jl.utils.SysConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Objects;

@RestController
@CrossOrigin
@RequestMapping("/oauth")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @GetMapping("/verify/{accessToken}")
    public String verifyUser(@PathVariable("accessToken") String accessToken) {

        if (StringUtils.isEmpty(accessToken)) {
            return ResultUtil.result(SysConf.ERROR, "token令牌未被识别");
        } else {
            String[] info = accessToken.split(" ");
            String userUid = info[0];

            // 校验redis内存储的token
            String redisKey = "token:" + userUid;
            if (!Objects.equals(stringRedisTemplate.opsForValue().get(redisKey), accessToken)) {
                return ResultUtil.result(SysConf.ERROR, "登录信息已过期，请重新登录！");
            }

            User user = userService.getUserByUid(userUid);
            user.setPassWord("");
            user.setPhotoUrl(user.getAvatar());
            String json = JsonUtils.objectToJson(user);
            Map<String, Object> map = JsonUtils.jsonToMap(json);
            return ResultUtil.result(SysConf.SUCCESS, map);
        }
    }
}
