package com.jl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jl.pojo.Comment;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WebCommentMapper extends BaseMapper<Comment> {
}
