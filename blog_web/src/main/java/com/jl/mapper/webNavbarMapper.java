package com.jl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jl.pojo.WebNavbar;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface webNavbarMapper extends BaseMapper<WebNavbar> {
}
