package com.jl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jl.pojo.Picture;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author YanYong
 * @Date 2022/3/10 17:13
 */
@Mapper
public interface PictureMapper extends BaseMapper<Picture> {

    /**
     * 保存标题图片
     *
     * @param picture
     */
    void savePicture(Picture picture);
}
