package com.jl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jl.pojo.Blog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: BlogMapper
 * @Description:
 * @Date: 2022/2/21 19:32
 * @Author YanYong
 * @Since JDK 1.8
 */
@Mapper
public interface BlogMapper extends BaseMapper<Blog> {
    @Select({"<script>",
            "SELECT DATE_FORMAT(create_time, '%Y-%m') m FROM t_blog GROUP BY m",
            "ORDER BY m",
            "</script>"})
    List<Date> selectSortList();
}
