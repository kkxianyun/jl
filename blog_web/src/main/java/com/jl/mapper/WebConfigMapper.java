package com.jl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jl.pojo.WebConfig;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WebConfigMapper extends BaseMapper<WebConfig> {
}
