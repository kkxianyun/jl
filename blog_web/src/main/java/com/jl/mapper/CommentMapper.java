package com.jl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jl.pojo.Comment;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName: CommentMapper
 * @Description:
 * @Date: 2022/2/23 15:07
 * @Author YanYong
 * @Since JDK 1.8
 */
@Mapper
public interface CommentMapper extends BaseMapper<Comment> {
}
