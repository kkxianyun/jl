package com.jl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jl.pojo.BlogSort;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author YanYong
 * @Date 2022/3/1 12:09
 */
@Mapper
public interface BlogSortMapper extends BaseMapper<BlogSort> {
}
