package com.jl.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
//        super.configure();
        httpSecurity.cors()
                .and()
                .csrf()
                .disable()
                // 基于token，所以不需要session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()

                .authorizeRequests()
                .antMatchers(
                        "/login/**",
                        "/index/**",
                        "/oauth/**",
                        "/content/**",
                        "/web/comment/**",
                        "/sysDictData/**",
                        "/search/**",
                        "/classify/**",
                        "/file/**",
                        "/sort/**",
                        "/tag/**")
                .permitAll()
                .anyRequest().authenticated();

        //禁用缓存
        httpSecurity.headers().cacheControl();
    }
}
