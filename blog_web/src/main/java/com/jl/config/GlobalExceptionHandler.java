package com.jl.config;

import com.jl.utils.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger log= LoggerFactory.getLogger(GlobalExceptionHandler.class);
    /**@ExceptionHandler注解描述的方法为异常处理方法,注解中定义的异常类型为方法可以处理的异常类型.*/
    @ExceptionHandler(RuntimeException.class)
    public String doHandleRuntimeException(RuntimeException e){
        e.printStackTrace();
        log.error("exception msg is {}",e.getMessage());
        return ResultUtil.errorWithData(e.getMessage());
    }
}
