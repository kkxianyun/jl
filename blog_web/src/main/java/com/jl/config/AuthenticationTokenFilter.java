package com.jl.config;

import com.jl.utils.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * 登录前的拦截器
 */
@Component
public class AuthenticationTokenFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String authorization = request.getHeader("Authorization");
//        if (authorization == null){
//            throw new RuntimeException("用户未登录");
//        }
        if (StringUtils.isNotEmpty(authorization)) {
            String[] strings = authorization.split(" ");
            request.setAttribute("userUid", strings[0]);
            request.setAttribute("userName", strings[1]);
        }
        filterChain.doFilter(request, response);
    }
}
