import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import ElementUI from '../components/ElementUI.vue'
import Home from '../components/Home.vue'
import User from '../components/user/user.vue'
import Comment from '../components/message/Comment.vue'
import BlogSort from '../components/blog/BlogSort.vue'
import Tag from '../components/blog/Tag.vue'
import Feedback from '../components/message/Feedback.vue'
import Blog from '../components/blog/Blog.vue'
import AddBlog from '../components/blog/AddBlog.vue'
import UpdateBlog from '../components/blog/UpdateBlog.vue'

//使用路由机制
Vue.use(VueRouter)
const routes = [
  {path: '/', redirect: '/login'},
  {path: '/login', component: Login, meta:{hidden: true, title:"金陵博客后台管理-用户登录"}},
  {path: '/elementUI', component: ElementUI},
  {path: '/home', component: Home, children:[
    {path: '/user', component: User, meta:{hidden: true, title:"金陵博客后台管理-用户管理"}},
    {path: '/feedback', component: Feedback, meta:{hidden: true, title:"金陵博客后台管理-反馈管理"}},
    {path: '/comment', component: Comment, meta:{hidden: true, title:"金陵博客后台管理-评论管理"}},
    {path: '/blogsort', component: BlogSort, meta:{hidden: true, title:"金陵博客后台管理-博客分类"}},
    {path: '/tag', component: Tag, meta:{hidden: true, title:"金陵博客后台管理-博客标签"}},
    {path: '/blog', component: Blog, meta:{hidden: true, title:"金陵博客后台管理-博客列表"}},
    {path: '/blog/addBlog', component: AddBlog, meta:{hidden: true, title:"金陵博客后台管理-添加博客"}},
    {path: '/blog/updateBlog', component: UpdateBlog, meta:{hidden: true, title:"金陵博客后台管理-编辑博客"}},
  ],meta:{hidden: true, title:"金陵博客后台管理"}},
]

//路由导航守卫!!!!!!!

const router = new VueRouter({
  routes
})
router.beforeEach((to, from, next) => {
  /* 路由发生变化修改页面title */
  if (to.meta.title) {
    document.title = to.meta.title
  }
  next()
})

export default router
