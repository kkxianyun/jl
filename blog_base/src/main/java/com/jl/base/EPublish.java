package com.jl.base;

/**
 * @ClassName: EPublish
 * @Description: 发布状态枚举类
 * @Date: 2022/2/21 22:52
 * @Author YanYong
 * @Since JDK 1.8
 */
public class EPublish {

    /**
     * 发布
     */
    public static final String PUBLISH = "1";

    /**
     * 下架
     */
    public static final String NO_PUBLISH = "0";
}
