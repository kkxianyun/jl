package com.jl.base;


import com.jl.validator.Messages;
import com.jl.validator.annotion.LongNotNull;
import com.jl.validator.group.GetList;
import lombok.Data;

/**
 * PageVO  用于分页
 */
@Data
public class PageInfo<T> {

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 当前页
     */
    @LongNotNull(groups = {GetList.class}, message = Messages.PAGE_NOT_NULL)
    private Long currentPage;

    /**
     * 页大小
     */
    @LongNotNull(groups = {GetList.class}, message = Messages.SIZE_NOT_NULL)
    private Long pageSize;
}
