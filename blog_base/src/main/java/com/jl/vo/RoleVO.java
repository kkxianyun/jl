package com.jl.vo;

import com.jl.base.BaseVO;
import com.jl.validator.annotion.NotBlank;
import com.jl.validator.group.Insert;
import com.jl.validator.group.Update;
import lombok.Data;

/**
 * <p>
 * RoleVO
 * </p>
 */
@Data
public class RoleVO extends BaseVO<RoleVO> {


    /**
     * 角色名称
     */
    @NotBlank(groups = {Insert.class, Update.class})
    private String roleName;

    /**
     * 介绍
     */
    private String summary;

    /**
     * 该角色所能管辖的区域
     */
    private String categoryMenuUids;

}
