package com.jl.vo;

import com.jl.base.BaseVO;
import com.jl.validator.annotion.NotBlank;
import com.jl.validator.group.Insert;
import com.jl.validator.group.Update;
import lombok.Data;
import lombok.ToString;

/**
 * ResourceSortVO
 */
@ToString
@Data
public class ResourceSortVO extends BaseVO<ResourceSortVO> {

    /**
     * 分类名
     */
    @NotBlank(groups = {Insert.class, Update.class})
    private String sortName;
    /**
     * 分类介绍
     */
    private String content;

    /**
     * 分类图片UID
     */
    private String fileUid;

    /**
     * 排序字段
     */
    private Integer sort;

    /**
     * 无参构造方法
     */
    ResourceSortVO() {

    }

}
