package com.jl.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ImageVO {

    /**
     * 动态变化的路径
     */
    private String virtualPath;

    /**
     * 网络地址
     */
    private String urlPath;

    /**
     * 图片名称    uuid.jpg
     */
    private String fileName;

    /**
     * 标题图UID
     */
    private String fileUid;

}
