# jl 

#### 介绍
这是一个传统的博客系统，主要用于计算机技术方面的文章分享，以及发布一些心得感受。前台可以进行博客浏览、搜索、点赞、收藏、评论、反馈以及浏览热门博客等功能，后台拥有对用户进行禁言、停用账户、重置密码，对评论进行删除，对博客进行编辑、修改、发布等功能。

#### 项目目录

blog_admin:提供管理端API接口服务 
blog_base:提供了pojo类和vo类 
blog_web:提供用户端API接口服务  
sql:数据库文件 
vue_jinling_web: 前台前端工程
vue_jladmin_web: 后台前端工程

#### 项目的启动运行

1.将sql文件中的blog_data.sql文件导入到自己本地MySQL数据库中。
###
2.修改blog_admin工程和blog_web工程下的application.yml文件：
1）MySQL相关配置，将端口号，用户名和密码等修改为本地MySQL的相关参数。
2）Redis相关配置，将ip地址配置为自己安装redis的虚拟机ip地址。
###
3.启动blog_admin工程，启动blog_web工程（确保redis已启动，否则会报连接redis错误）
###
4.在终端打开 vue_jinling_web，使用npm run dev命令运行该vue工程；在终端打开 vue_jladmin_web，使用npm run serve命令运行该vue工程。
###
5.在浏览器打开 http://localhost:9527 (自动弹出)访问博客前台，在浏览器打开 http://localhost:8080 访问博客管理后台。前台可自行注册账号再登录，亦可使用账号jinling2022，密码123456登录；后台可使用账号admin，密码admin123456登录。